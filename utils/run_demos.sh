#!/bin/bash
#

while [[ $# -gt 0 ]]; do
  case $1 in
    -f|--filter-regex)
      filter_regex="$2"
      shift # past argument
      shift # past value
      ;;
    -s|--single)
      single="$2"
      shift # past argument
      shift # past value
      ;;
    -r|--report-file)
      report_file="$2"
      shift # past argument
      shift # past value
      ;;
    -n|--mpi-n)
      mpi_n="$2"
      shift # past argument
      shift # past value
      ;;
    *)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

# Default values
: ${filter_regex:=".*"}
: ${single:=""}
: ${report_file="utils/execution_report.txt"}
: ${mpi_n:="1"}

# Process input parameters into variables to be used
report_file=$(realpath $report_file)
[ "$mpi_n" -gt "1" ] && mpi_cmd="mpirun -n $mpi_n" || mpi_cmd=""
[ ! -z "$single" ] && filter_regex=".*"

# INFO
[ "$filter_regex" != ".*" ] && echo "INFO: Filter demos with: $filter_regex"
[ ! -z "$single" ]          && echo "INFO: single demo to be run: $single"
echo "INFO: Saving output to $report_file"
echo "INFO: Processors to be used: $mpi_n"

tail -f $report_file &

>$report_file

date                                           >$report_file 2>&1
echo                                          >>$report_file 2>&1
echo                                          >>$report_file 2>&1

i=0
for t in $(find demo -name "demo*.py" | sort)
do
    [[ $t =~ $filter_regex ]] || continue

    dirname=$(dirname $t)
    basename=$(basename $t)

    if [ ! -z "$single" ] && [ "$dirname" != "${single%/}" ]
    then
        continue
    fi

    let i=i+1    
    cd $dirname
    
    # echo INFO: $(date) ::: $i ::: Working on $t
    
    echo ------------------ $t                >>$report_file 2>&1
    echo "((( $i )))"                         >>$report_file 2>&1
    echo "image: $HEF_Acoustics_image"        >>$report_file 2>&1
    echo $(date)                              >>$report_file 2>&1
    { time $mpi_cmd python3 -u $basename ; }  >>$report_file 2>&1
    exit_code=$?
    echo                                      >>$report_file 2>&1
    echo INFO: exit code: $exit_code          >>$report_file 2>&1
    echo                                      >>$report_file 2>&1
    echo                                      >>$report_file 2>&1
    cd - > /dev/null    
done

echo ------------------ DONE $0               >>$report_file 2>&1
date                                          >>$report_file 2>&1

pkill -P $$
