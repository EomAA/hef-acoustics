#!/bin/bash
#

while [[ $# -gt 0 ]]; do
  case $1 in
    -f|--filter-regex)
      filter_regex="$2"
      shift # past argument
      shift # past value
      ;;
    -s|--single)
      single="$2"
      shift # past argument
      shift # past value
      ;;
    *)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

# Default values
: ${filter_regex:=".*"}
: ${single:=""}

# Process input parameters into variables to be used
[ ! -z "$single" ] && filter_regex=".*"

# INFO
[ "$filter_regex" != ".*" ] && echo "INFO: Filter demos with: $filter_regex"
[ ! -z "$single" ]          && echo "INFO: single demo to be run: $single"

i=0
for t in $(find demo -name "screenshots*.py" | sort)
do
    [[ $t =~ $filter_regex ]] || continue

    dirname=$(dirname $t)
    basename=$(basename $t)

    if [ ! -z "$single" ] && [ "$dirname" != "${single%/}" ]
    then
        continue
    fi

    let i=i+1    
    cd $dirname
    
    echo ------------------
    echo INFO: $(date) ::: $i ::: Working on $t
    pvbatch $basename 
    cd - > /dev/null
done

echo ------------------
echo DONE: $0
