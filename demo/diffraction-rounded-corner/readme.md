#  Diffraction from a rounded corner 

directory name: **diffraction-rounded-corner** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency
- sweep for the radius of the rounded corner
- local mesh refinement


## Screenshots
<img src="demo/diffraction-rounded-corner/results/screenshot_1.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_2.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_3.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_4.png" height="120">
<img src="demo/diffraction-rounded-corner/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/diffraction-rounded-corner/demo.py)
- [`screenshots.py`](demo/diffraction-rounded-corner/screenshots.py)


[pvsm paraview file](demo/diffraction-rounded-corner/results/vis.pvsm)



