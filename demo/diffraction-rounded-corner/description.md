# Diffraction from a rounded corner

- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency
- sweep for the radius of the rounded corner
- local mesh refinement
