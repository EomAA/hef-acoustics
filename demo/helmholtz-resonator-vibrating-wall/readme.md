#  Helmholtz resonator (vibrating wall) 

directory name: **helmholtz-resonator-vibrating-wall** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- wall velocity boundary condition
- frequency sweep
- resonance
- radiation

## Screenshots
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-vibrating-wall/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-vibrating-wall/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-vibrating-wall/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-vibrating-wall/results/vis.pvsm)



