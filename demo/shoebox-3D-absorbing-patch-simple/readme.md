#  Illustrate admittance boundary condition (first approach) 

directory name: **shoebox-3D-absorbing-patch-simple** 


- 3D shoebox geometry
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep

## Published work using this code

> Julio Octavio Rangel Flores, "Estudio numérico de régimen de baja
> frecuencia en medición de absorción en cámara reverberante, aplicado
> al problema de ruido urbano en viviendas", Bachelor dissertation,
> Licenciatura en Ingeniería Eléctrica y Electrónica, Facultad de
> Ingeniería, UNAM, México, 2019. (Spanish)


## Screenshots
<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_1.png" height="120">
<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_2.png" height="120">
<img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-simple/demo.py)
- [`screenshots.py`](demo/shoebox-3D-absorbing-patch-simple/screenshots.py)


[pvsm paraview file](demo/shoebox-3D-absorbing-patch-simple/results/vis.pvsm)



