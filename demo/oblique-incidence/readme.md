#  Beam in oblique incidence to another media 

directory name: **oblique-incidence** 


- 2D rectangular geometry
- oblique incidence
- overriding `k_expression`
- reflection
- refraction
- anechoic walls
- absorbing region
- gaussian beam
- microphone (receiving transducer)
- fixed frequency
- angle sweep
- ultrasound
- multiple media
- local mesh refinement

## Published work using this code

> Alejandro Ortega-Aguilar, Roberto Velasco-Segura, Augusto
> García-Valenzuela, Eduardo Sandoval-Romero, "Numerical simulation of
> ultrasound oblique reflection in a 2D gas-gas interface",
> Proceedings of Meetings on Acoustics 42, 7-11 December 2020.

## Screenshots
<img src="demo/oblique-incidence/results/screenshot_1.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_2.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_3.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_4.png" height="120">
<img src="demo/oblique-incidence/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/oblique-incidence/demo.py)
- [`screenshots.py`](demo/oblique-incidence/screenshots.py)


[pvsm paraview file](demo/oblique-incidence/results/vis.pvsm)



