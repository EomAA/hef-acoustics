#  Pulsating sphere (using gmsh) 

directory name: **pulsating-sphere-gmsh** 


- 3D free space geometry
- single octant
- usign gmsh (mesh refinement)
- anechoic boundary condition
- fixed frequency
- radiation
- overriding `k_expression`
- analytic solution: (TODO: implement) pulsating sphere

## Additional dependencies

Only to regenerate the mesh file `xdmf`
- pip
  - h5py
  - gmsh
  - meshio
- apt
  - gmsh

## Screenshots
<img src="demo/pulsating-sphere-gmsh/results/screenshot_1.png" height="120">
<img src="demo/pulsating-sphere-gmsh/results/screenshot_2.png" height="120">
<img src="demo/pulsating-sphere-gmsh/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-sphere-gmsh/demo.py)
- [`screenshots.py`](demo/pulsating-sphere-gmsh/screenshots.py)


[pvsm paraview file](demo/pulsating-sphere-gmsh/results/vis.pvsm)



