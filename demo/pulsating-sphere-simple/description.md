# Pulsating sphere (first approach)

- **not working**
- 3D free space geometry
- anechoic boundary condition
- fixed frequency
- pulsating sphere
- overriding `k_expression`
- local mesh refinement
- radiation

## Known issues

### The sphere cannot be small

The problem is this. If the parameter `initial_mesh_resolution` is
small (as it should be), say 40, this means big tethraedra. And, if
the raduis of the sphere is small, say `R=0.25`. Then, the meshing
process ignores the sphere, and makes the mesh without any hole un the
center.

This is strange, because after the mesh refinement the sphere looks
much larger than the tethraedra. But that is because of the additional
refinement.

It is not possible to start with a high value of
`initial_mesh_resolution`, an then unrefine. FEniCS does not have
tools to unrefine meshes.

The alternative seems to be to use `gmsh`, which have an option to de
define the mesh refinement for specific surfaces.

### ERROR: Out of memory

Even with a big sphere. Something seems to requiere more memory than
the example of the point source in 3D. In consequence, it cannot be
used the same amount of refinement here.

The alternative seems to be to take advantage of the symmetries, and
simulate only one octant, to reduce the domain to one eigth of the
original domain.
