from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
import hef

# For 3D problems is sometimes good idea to use `dolfin_adjoint`, if
# available you can change this line
hef.engine = "fenics"
# to
# hef.engine = "dolfin_adjoint"

from hef import solver as H
from hef.utils import refine_mesh_on

# Forcing velocity amplitude
U0 = 6.0

# Sphere radius
R = 0.5

initial_mesh_resolution = 35

domain = (
    Box(
        Point(-2.0, -2.0, -2.0),
        Point(2.0, 2.0, 2.0) )
    - Sphere(
        Point(0.0, 0.0, 0.0),
        R) ) # [m]

mesh = generate_mesh(domain, initial_mesh_resolution)

mesh = refine_mesh_on(
    lambda x : ( x[0]**2 + x[1]**2 + x[2]**2)**0.5 < 1,
    mesh = mesh )

H.mesh = mesh

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')
fid_pa = File(f'{output_dir}/pa.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        if  abs(x[0]) > 1 or abs(x[1]) > 1 or abs(x[2]) > 1 :
            # This is k_i
            value[1] = - 20*max(
                [abs(x[0]) - 1.0,
                 abs(x[1]) - 1.0,
                 abs(x[2]) - 1.0])**2
        else :
            # This is k_i
            value[1] = 0
    def value_shape(self):
        return (2,)

# Pulsating sphere
class Sphere_source(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 between(x[0], (-1, 1)) and
                 between(x[1], (-1, 1)) and
                 between(x[2], (-1, 1)) )

sphere_source = Sphere_source()

H.U0_expression = Constant((U0, 0.0))

# If you have enough memory in your system, it is better to use
# `V_degree=2` here.
H.init(
    boundary_U0=sphere_source,
    V_degree=1)

H.k_expression = k_Expression(
    degree=0)

H.update_frequency(1000)

A, b = H.Ab_assemble()

p_c = Function(H.V, name="p")
solve(A, p_c.vector(), b)

fid_p << (p_c,   float(H.f))
fid_k << (H.k_c, float(H.f))

