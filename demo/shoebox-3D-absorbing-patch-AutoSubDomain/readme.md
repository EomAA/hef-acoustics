#  Illustrate admittance boundary condition (`AutoSubDomains`) 

directory name: **shoebox-3D-absorbing-patch-AutoSubDomain** 


- **not yet implemented**
- 3D shoebox geometry
- mesh generated with gmsh
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep
- define regions: using `AutoSubDomains`

## TODO

This example is not yet implemented. It should be similar to
shoebox-3D-absorbing-patch-simple, but using `AutoSubDomains` to have a well defined patch.


Scripts:
- [`demo.py`](demo/shoebox-3D-absorbing-patch-AutoSubDomain/demo.py)




