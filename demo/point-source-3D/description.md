# Point source in free space 3D 

- 3D free space geometry
- anechoic boundary condition
- fixed frequency
- point source
- overriding `k_expression`
- local mesh refinement
- radiation
- results normalization

## Running

It is advised to run this demo using MPI, as
```bash
mpirun -n 4 python3 demo.py
```

## Issues

- minimal paraview problem: issue #36
