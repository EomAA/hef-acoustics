#  Point source in free space 3D  

directory name: **point-source-3D** 


- 3D free space geometry
- anechoic boundary condition
- fixed frequency
- point source
- overriding `k_expression`
- local mesh refinement
- radiation
- results normalization

## Running

It is advised to run this demo using MPI, as
```bash
mpirun -n 4 python3 demo.py
```

## Issues

- minimal paraview problem: issue #36


## Screenshots
<img src="demo/point-source-3D/results/screenshot_1.png" height="120">
<img src="demo/point-source-3D/results/screenshot_2.png" height="120">
<img src="demo/point-source-3D/results/screenshot_3.png" height="120">
<img src="demo/point-source-3D/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/point-source-3D/demo.py)
- [`screenshots.py`](demo/point-source-3D/screenshots.py)


[pvsm paraview file](demo/point-source-3D/results/vis.pvsm)



