#  Helmholtz resonator (axisymmetric) 

directory name: **helmholtz-resonator-axisymmetric** 

- cylinder geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- wall velocity boundary condition
- frequency sweep
- resonance
- radiation

## Screenshots
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-axisymmetric/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-axisymmetric/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-axisymmetric/results/vis.pvsm)



