# Comparison of an absorbing layer with a boundary condition for the admittance

- 2D shoebox geometry
- absorbing layer
- wall admittance boundary condition
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep

## Geometry

There are 4 ducts, two of them with an absorbing layer at the right
end, which is based in a quadratically increasing value of $`k_i`$,
the imaginary part of the wave number.

For the other two ducts, the right end (see the boundary where the
"subdomains" field equals 1) have a value for the admittance matching
the impedance of the media inside the ducts. That is,

```math
Y = \frac{1}{\rho_0 c}
```

For each case one of the ducts have a vertical wall at the right end,
and the other have a wedge.

## Observations

The magnitude of the pressure is a straight horizontal line for the
cases where there is no reflection on the right end, which is the
desired behaviour. If a reflection is present, a ripple appear over
this line.

For the absorbing layer (ducts 1 and 3), the length the wave takes to
decay (0.75 m) seems to be independent on the frequency. The absorbing
layer works fine for frequencies over 440 Hz, whose wavelength is 0.78
m, which is approximatelly the previously mentioned length. This is
not affected by the presence of the wedge.

The admittance condition works penfectly, even for low frequencies in
normal incidence (duct 2). In the case of the wedge (oblique
incidence, duct 4), there is a little reflection.

TODO: add a duct which has a wedge and both an absorbing layer, and
the condition for the admittance.
