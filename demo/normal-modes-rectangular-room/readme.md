#  Normal modes in 2D rectangular domain 

directory name: **normal-modes-rectangular-room** 


- 2D shoebox geometry
- point source
- reflecting walls
- resonance
- nomal modes
- frequency sweep

## Screenshots
<img src="demo/normal-modes-rectangular-room/results/screenshot_1.png" height="120">
<img src="demo/normal-modes-rectangular-room/results/screenshot_2.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-rectangular-room/demo.py)
- [`screenshots.py`](demo/normal-modes-rectangular-room/screenshots.py)


[pvsm paraview file](demo/normal-modes-rectangular-room/results/vis.pvsm)



