from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

domain = Rectangle( Point(0.0, 0.0),
                    Point(1.1, 1.0) )  # [m]

source_location = Point(0.7, 0.2)

H.mesh = generate_mesh(domain, 100)

H.init()

f_list = np.arange(400, 500, 5)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p  = File(f'{output_dir}/p.pvd')

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = PointSource(H.V, source_location, 1)

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))
