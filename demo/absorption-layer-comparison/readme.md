#  Comparison of different options for an absorbing layer 

directory name: **absorption-layer-comparison** 


- 2D shoebox geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep

## Geometry

There are 5 ducts, all of them have absorbing layers at the right
end. However, the value of $`k_i`$, the imaginary part of the wave
vector, have a different dependence with respect to the x coordinate for
each case.
- constant
- linear
- quadratic
- cubic
- quartic

## Observations

The magnitude of the pressure is a straight horizontal line for the
cases where there is no reflection on the right end, which is the
desired behaviour. If a reflection is present, a ripple appear over
this line.

All cases perform better for higher frequencies, than for lower
frequencies.

Quadratic, cubic, and quartic cases behave significantly better than
the linear and the constant cases.




## Screenshots
<img src="demo/absorption-layer-comparison/results/screenshot_1.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_2.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_3.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_4.png" height="120">
<img src="demo/absorption-layer-comparison/results/screenshot_5.png" height="120">


Scripts:
- [`demo.py`](demo/absorption-layer-comparison/demo.py)
- [`screenshots.py`](demo/absorption-layer-comparison/screenshots.py)


[pvsm paraview file](demo/absorption-layer-comparison/results/vis.pvsm)



