#  Helmholtz resonator (point source) 

directory name: **helmholtz-resonator-point-source** 


- **not working**
- 2D rectangular geometry
- anechoic walls
- point source
- absorbing region
- overriding `k_expression`
- frequency sweep
- resonance
- radiation

## Known issues

The condition of constant pressure in the resonator is broken. This is
posibly related to the shift in resonance frequency.

## Screenshots
<img src="demo/helmholtz-resonator-point-source/results/screenshot_1.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_2.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_3.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_4.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_5.png" height="120">
<img src="demo/helmholtz-resonator-point-source/results/screenshot_6.png" height="120">


Scripts:
- [`demo.py`](demo/helmholtz-resonator-point-source/demo.py)
- [`screenshots.py`](demo/helmholtz-resonator-point-source/screenshots.py)


[pvsm paraview file](demo/helmholtz-resonator-point-source/results/vis.pvsm)



