# Illustrate different options to define regions with specific acoustic properties

- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: creating a subclass of `SubDomain`
- not so well defined materials interface

## Different options

There are a few different ways to "define regions", to be able to set
different physical properties. See the other demos for
`define-regions-`
