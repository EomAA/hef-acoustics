#  Illustrate different options to define regions with specific acoustic properties 

directory name: **define-regions-SubDomain** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: creating a subclass of `SubDomain`
- not so well defined materials interface

## Different options

There are a few different ways to "define regions", to be able to set
different physical properties. See the other demos for
`define-regions-`


## Screenshots
<img src="demo/define-regions-SubDomain/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-SubDomain/demo.py)
- [`screenshots.py`](demo/define-regions-SubDomain/screenshots.py)


[pvsm paraview file](demo/define-regions-SubDomain/results/vis.pvsm)



