# Set absorption value depending on frequency

- 2D shoebox geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- k_i depending on frequency
- frequency sweep
