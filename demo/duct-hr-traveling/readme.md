# `duct-hr-traveling`



## Screenshots
<img src="demo/duct-hr-traveling/results/screenshot_1.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_2.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_3.png" height="120">
<img src="demo/duct-hr-traveling/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/duct-hr-traveling/demo.py)
- [`screenshots.py`](demo/duct-hr-traveling/screenshots.py)


[pvsm paraview file](demo/duct-hr-traveling/results/vis.pvsm)



