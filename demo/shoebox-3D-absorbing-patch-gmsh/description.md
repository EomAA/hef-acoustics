# Illustrate admittance boundary condition (gmsh)

- **not yet implemented**
- 3D shoebox geometry
- mesh generated with gmsh
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep

## TODO

This example is not yet implemented. It should be similar to
shoebox-3D-absorbing-patch-simple, but using gmsh to have a well defined patch.
