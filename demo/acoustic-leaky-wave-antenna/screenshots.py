# This script is inteded to be run with pvbatch

from paraview.simple import *

# load state
LoadState(
    './results/vis.pvsm',
    LoadStateDataFileOptions='Search files under specified directory',
    DataDirectory='./results')

for i in range(1,9) :
    layout = GetLayoutByName(f"Layout #{i}")
    SetActiveView(GetViewsInLayout(layout)[0])
    Render()
    layout.SetSize(1411, 821)
    SaveScreenshot(
        f'./results/screenshot_{i}.png',
        layout,
        SaveAllViews=1,    
        ImageResolution=[1411, 821])

