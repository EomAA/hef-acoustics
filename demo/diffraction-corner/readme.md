#  Diffraction from a corner 

directory name: **diffraction-corner** 


- 2D rectangular geometry
- anechoic walls
- absorbing region
- overriding `k_expression`
- gaussian beam
- diffraction
- Dirichlet boundary condition
- fixed frequency


## Screenshots
<img src="demo/diffraction-corner/results/screenshot_1.png" height="120">
<img src="demo/diffraction-corner/results/screenshot_2.png" height="120">
<img src="demo/diffraction-corner/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/diffraction-corner/demo.py)
- [`screenshots.py`](demo/diffraction-corner/screenshots.py)


[pvsm paraview file](demo/diffraction-corner/results/vis.pvsm)



