from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# Speed of sound
H.c = 343 # [m/s]

# Frequency
H.f = 1000

# Domain [m]
width  = 5
height = 5

beam_width = height/4

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class k_Expression(UserExpression):

    def __init__(self, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r = k_r
        self.k_i = k_i

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        x0b = width/4
        x1b = height/4
        # value[1] is k_i
        if x[0] > x0b or abs(x[1]) > x1b :
            if x[0] > 0 :
                value[1] = - 5*max(
                    [abs(x[0]) - x0b,
                     abs(x[1]) - x1b])**2
            else :
                value[1] = - 5*(abs(x[1]) - x1b)**2
        else :
            value[1] = 0
    def value_shape(self):
        return (2,)

domain = Rectangle(
    Point(-width/2, -height/2),
    Point( width/2,  height/2) ) # [m]

wall = Rectangle(
    Point(-width/2,    -height/2),
    Point(-width/8, 0.0) ) # [m]

domain = domain - wall

mesh = generate_mesh(domain, 250)

H.k_expression = k_Expression(
    degree=0 )

H.mesh = mesh

class Emitter(SubDomain):
    def inside(self, x, on_boundary) :
        return ( on_boundary and near(x[0], -width/2) )

emitter = Emitter()

P0_expression = Expression(
    ('exp(-0.5*pow((x[1] - mu)/s,N))', '0.0'),
    mu=0,
    s=beam_width/2.0,
    N=2,
    degree=2 )

H.init()
bc = [
    DirichletBC(
        H.V,
        P0_expression,
        emitter)]

A, b     = H.Ab_assemble(bc)
p_c      = Function(H.V, name="p")
solve(A, p_c.vector(), b)
p_r, p_i = p_c.split()
k_r, k_i = H.k_c.split()

fid_p << p_c
fid_k << H.k_c

