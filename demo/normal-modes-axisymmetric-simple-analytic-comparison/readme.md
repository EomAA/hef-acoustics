#  Normal modes in simple axisymmetric domain (comparison with analytical solution) 

directory name: **normal-modes-axisymmetric-simple-analytic-comparison** 


- Petri dish geometry
- **modified speed of sound**
- point source
- reflecting walls
- normal modes
- resonance
- axisymmetic
- analytic solution: Bessel functions (for specific frequencies)
- frequency sweep

## Screenshots
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_1a.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_1b.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_3.png" height="120">
<img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-axisymmetric-simple-analytic-comparison/demo.py)
- [`screenshots.py`](demo/normal-modes-axisymmetric-simple-analytic-comparison/screenshots.py)


[pvsm paraview file](demo/normal-modes-axisymmetric-simple-analytic-comparison/results/vis.pvsm)



