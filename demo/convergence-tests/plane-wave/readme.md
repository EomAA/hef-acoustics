#  Convergence test for "plane wave propagation" 

directory name: **convergence-tests/plane-wave** 


This code is an extenssion of [plane-wave](demo/plane-wave).

## Additional dependecies

pip
- pyyaml


## Screenshots
<img src="demo/convergence-tests/plane-wave/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/plane-wave/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/plane-wave/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/plane-wave/demo.py)
- [`screenshots.py`](demo/convergence-tests/plane-wave/screenshots.py)


[pvsm paraview file](demo/convergence-tests/plane-wave/results/vis.pvsm)



