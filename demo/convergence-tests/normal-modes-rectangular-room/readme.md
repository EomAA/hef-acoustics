#  Convergence test for "Normal modes in 2D rectangular domain" 

directory name: **convergence-tests/normal-modes-rectangular-room** 


This code is an extenssion of [normal-modes-rectangular-room-analytic-comparison](demo/normal-modes-rectangular-room-analytic-comparison)

## Additional dependecies

pip
- pyyaml



## Screenshots
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/normal-modes-rectangular-room/demo.py)
- [`screenshots.py`](demo/convergence-tests/normal-modes-rectangular-room/screenshots.py)


[pvsm paraview file](demo/convergence-tests/normal-modes-rectangular-room/results/vis.pvsm)



