# This script is inteded to be run with pvbatch

from paraview.simple import *

# load state
LoadState(
    './results/vis.pvsm',
    LoadStateDataFileOptions='Search files under specified directory',
    DataDirectory='./results')

# sources = GetSources()
# for k in sources :
#     try :
#         tsteps = sources[k].TimestepValues
#         break
#     except :
#         pass

layout = GetLayoutByName("Layout #1")
layout.SetSize(1411, 821)
SetActiveView(GetViewsInLayout(layout)[0])
Render()
SaveScreenshot(
    './results/screenshot_1.png',
    layout,
    SaveAllViews=1,    
    ImageResolution=[1411, 821])

layout = GetLayoutByName("Layout #3")
layout.SetSize(1402, 786)
SetActiveView(GetViewsInLayout(layout)[0])
Render()

# for view in GetViewsInLayout(layout) :
#     view.ViewTime = tsteps[3]
#     SetActiveView(view)
#     Render()

SaveScreenshot(
    './results/screenshot_2.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])

# for view in GetViewsInLayout(layout) :
#     view.ViewTime = tsteps[6]
#     SetActiveView(view)
#     Render()

# SaveScreenshot(
#     './results/screenshot_2b.png',
#     layout,
#     SaveAllViews=1,
#     ImageResolution=[1402, 786])

layout = GetLayoutByName("Layout #4")
layout.SetSize(1402, 786)
SetActiveView(GetViewsInLayout(layout)[0])
Render()
SaveScreenshot(
    './results/screenshot_3.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])

layout = GetLayoutByName("Layout #6")
SetActiveView(GetViewsInLayout(layout)[0])
layout.SetSize(1402, 786)
SaveScreenshot(
    './results/screenshot_4.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])
