#  Convergence test for "plane wave propagation" with a fixed mesh 

directory name: **convergence-tests/plane-wave-fixed-mesh** 


This code is an extenssion of [plane-wave](demo/plane-wave).

## Difference with other convergence tests

This code is a little variation over
[`convergence-tests/plane-wave`](demo/convergence-tests/plane-wave). The
difference is that here the mesh is fixed and `N` change because of a
change in frequency.

Results in this test are similar to those obtained in
[`convergence-tests/plane-wave`](demo/convergence-tests/plane-wave). However,
both $`N_1`$ and $`\eta`$ are a little larger.

The reason for these values is probably the following. If you take the
distance of a point inside the domain to a point in the boundary,
**measured in wavelengths**, as the frequency increases the value of
this distance gets higher. Of course this distance is not longer, it
only has more wavelengths.

Further exploration is needed to check if this explanation is correct.

## Additional dependecies

pip
- pyyaml


## Screenshots
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_1.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_2.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_3.png" height="120">
<img src="demo/convergence-tests/plane-wave-fixed-mesh/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/convergence-tests/plane-wave-fixed-mesh/demo.py)
- [`screenshots.py`](demo/convergence-tests/plane-wave-fixed-mesh/screenshots.py)


[pvsm paraview file](demo/convergence-tests/plane-wave-fixed-mesh/results/vis.pvsm)



