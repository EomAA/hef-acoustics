from dolfin import *
from mshr import *
import numpy as np
import sys
import pathlib
import sys
import itertools
import pandas as pd
from pprint import pprint
from scipy import stats
import yaml

# import HEF-Acoustics main module
from hef import solver as H

domain = Rectangle( Point(0.0, 0.0),
                    Point(5.1, 5.0) )  # [m]

# The error is believed to be related with this approach. One of the
# parameters of this study is `N` the approximate number of cells per
# wavelength. While the refinement is fixed here, the sweep over `N`
# is achieved using differet frequencies.
H.mesh = generate_mesh(domain, 50)

edge_size_list = [
    edge.length()
    for edge
    in edges(H.mesh) ]
cell_size_list = [
    (cell.inradius(), cell.circumradius())
    for cell
    in cells(H.mesh) ]

mesh_size_parameters = {
    "cell_inradius_mean"    : float(np.mean([ x[0] for x in cell_size_list])),
    "cell_inradius_std"     : float(np.std( [ x[0] for x in cell_size_list])),
    "cell_circumradius_mean": float(np.mean([ x[1] for x in cell_size_list])),
    "cell_circumradius_std" : float(np.std( [ x[1] for x in cell_size_list])),
    "mesh_edge_mean"        : float(np.mean(edge_size_list)),
    "mesh_edge_std"         : float(np.std( edge_size_list)) }

pprint(mesh_size_parameters)

# angle [degrees]
theta_list = {
    i: theta
    for (i, theta)
    in enumerate(
        list(np.arange(0,45,5)) + [45]) }

# If you use a lower degree you get a warning message from
# `dolfin.errornorm`
#
# *** Warning: Degree of exact solution may be inadequate for accurate result in errornorm.
analytic_solution = Expression(
    ("cos(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)",
     "sin(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)"),
    pi = np.pi,
    theta = 0,
    k = 0,
    omega = 0,
    t = 1,
    degree = 5)

H.init()

N_list = np.logspace(
    np.log10(2),
    np.log10(15),
    8)
wavelength_list = [
    N*mesh_size_parameters["mesh_edge_mean"]
    for N
    in N_list ]
f_list = [
    H.c/wavelength
    for wavelength
    in wavelength_list ]

test_data = {
    "i"             : [],
    "theta"         : [],
    "f"             : [],
    "error_L2_1"    : [],
    "error_L2_2"    : [],
    "error_percent" : [],
    "N"             : [],
    "wavelength"    : []}

i_prev = None
for n, (i, H.f) in enumerate(itertools.product(theta_list, f_list)) :

    theta = theta_list[i]
    
    if not i == i_prev :
        output_dir = f'./results/i_{i}'
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
        fid_p   = File(f'{output_dir}/p.pvd')
        fid_pa  = File(f'{output_dir}/pa.pvd')
        print(f"-"*20)

    H.update_frequency()

    # Recalculate the wavelength
    wavelength = H.c/H.f

    N = wavelength/mesh_size_parameters["mesh_edge_mean"]

    print(f"Working on run {n}:")
    print(f"    f = {H.f:0.2f} Hz, theta = {theta:0.2f}")
    print(f"    wavelength = {wavelength:0.2f} m, N = {N:0.2f}")

    analytic_solution.k     = H.k_expression.k_r
    analytic_solution.omega = H.omega
    analytic_solution.theta = theta

    bc = DirichletBC(
        H.V,
        analytic_solution,
        'on_boundary')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    fid_p << (p_c, float(N))

    pa_c = Function(H.V, name="pa")
    assign(pa_c,  interpolate(analytic_solution, H.V))
    fid_pa << (pa_c, float(N))

    theta_prev = theta

    error_L2_1 = (assemble(((p_c - pa_c)**2)*dx))**0.5
    error_L2_2 = errornorm(analytic_solution,p_c,"L2")
    ref = errornorm(
        analytic_solution,
        interpolate(Constant((0.0,0.0)), H.V),
        "L2")

    error_percent = 100*error_L2_2/ref

    test_data["i"].append(i)    
    test_data["theta"].append(theta)
    test_data["f"].append(H.f)
    test_data["error_L2_1"].append(error_L2_1)
    test_data["error_L2_2"].append(error_L2_2)
    test_data["error_percent"].append(error_percent)
    test_data["N"].append(N)
    test_data["wavelength"].append(wavelength)

    i_prev = i

df_test_data = pd.DataFrame(test_data)

df_test_data.to_csv(f'./results/test_data.csv')

df = df_test_data.pivot_table(
    index="N",
    columns="i",
    values="error_percent")

df = df.rename(
    columns={
        key: f"(i={key}) theta={item}"
        for (key, item)
        in theta_list.items() } )

df.to_csv(
    f'./results/error_percent+N+i.csv')

error_percet_avg = [
    (key, np.mean(row))
    for (key, row)
    in df.iterrows() ]

slope, intercept,_,_,_ = stats.linregress(
    [ np.log10(item[0])
      for item
      in error_percet_avg ],
    [ np.log10(item[1])
      for item
      in error_percet_avg ] )
eta = -slope

tendency = {
    "N" : [ item[0] for item in error_percet_avg ],
    "Ep" : [
        (10**intercept)*(item[0]**slope)
        for item
        in error_percet_avg ] }

N1 = (10**(-intercept))**(1/slope)

df_tendency = pd.DataFrame(tendency)
df_tendency.to_csv(f'./results/tendency.csv')

with open(f'./results/parameters.yaml', 'w') as fd :
    yaml.dump({
        "mesh_size_parameters": mesh_size_parameters,
        "convergence_order": float(slope),
        "N1": float(N1),
        "linregress_intercept" : float(intercept) },
              fd,
              default_flow_style=False)

with open(f'./results/parameters.csv', 'w') as fd :
    fd.write("parameter,value\n")
    fd.write(f"eta, {eta:0.2f}\n")
    fd.write(f"N1, {N1:0.2f}\n")
    fd.write(f"linregress_intercept, {intercept:0.2f}\n")
    
with open(f'./results/N1.csv', 'w') as fd :
    Ep_min = min(tendency["Ep"])
    Ep_max = max(tendency["Ep"])
    fd.write("N,Ep\n")
    fd.write(f"{N1:0.2f},{Ep_min:0.2f}\n")
    fd.write(f"{N1:0.2f},{Ep_max:0.2f}\n")
