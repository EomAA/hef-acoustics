# Convergence test for "pulsating tube"

This code is an extenssion of [pulsating-cylinder-cartesian](demo/pulsating-cylinder-cartesian)

## Wavelength

The variable `wavelength` in this case is the wave length of a plane
wave propagating in the same media. However, because of the axial
symmetry, this is not precisely the distance between two consecutive
maxima of a propagating wave in the radial direction, computed
numerically, or obtained from the analytic solution. Nevertheless,
this variable `wavelength` is considered to be representative enough
of such length to be used to meassure convergence rates.

## Axial symmetry

This simulation correspond to a system with axial symmetry: a
pulsating cylindrical tube. However, it is **not** coded to use the
axisymmetric feature of the `hef` module. The symmetry axis is not in
the domain, but it is transversal to the domain, the tube is
represented with a a circle in a plane cartesian domain.

## Integration domain

See that the integration to get the error between analytics and
numeric solutions is made in a reduced domain, excluding the part
where attenuation is present.

## No need for normalization

See that, unlike some ot the other convergence tests, no normalization
of the analytic solution was performed for this case. Both, the
amplitude of the numeric solution, and the corresponding to the
analytic expression, are determined by the magnitude `U0` used in the
boundary condition. This fact, is a validation of the implementation
of this kind of boundary condition.

## Additional dependecies

pip
- pyyaml
