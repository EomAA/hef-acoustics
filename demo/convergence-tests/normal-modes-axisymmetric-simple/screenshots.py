# This script is inteded to be run with pvbatch

from paraview.simple import *

# load state
LoadState(
    './results/vis.pvsm',
    LoadStateDataFileOptions='Search files under specified directory',
    DataDirectory='./results')

layout = GetLayoutByName("Layout #1")
SetActiveView(GetViewsInLayout(layout)[0])
Render()
layout.SetSize(1411, 821)
SaveScreenshot(
    './results/screenshot_1.png',
    layout,
    SaveAllViews=1,    
    ImageResolution=[1411, 821])

layout = GetLayoutByName("Layout #3")
SetActiveView(GetViewsInLayout(layout)[0])
Render()
layout.SetSize(1402, 786)
SaveScreenshot(
    './results/screenshot_2.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])

layout = GetLayoutByName("Layout #4")
SetActiveView(GetViewsInLayout(layout)[0])
Render()
layout.SetSize(1402, 786)
SaveScreenshot(
    './results/screenshot_3.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])

layout = GetLayoutByName("Layout #6")
SetActiveView(GetViewsInLayout(layout)[0])
layout.SetSize(1402, 786)
SaveScreenshot(
    './results/screenshot_4.png',
    layout,
    SaveAllViews=1,
    ImageResolution=[1402, 786])

