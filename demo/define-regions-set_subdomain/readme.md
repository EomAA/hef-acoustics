#  Illustrate different options to define regions with specific acoustic properties 

directory name: **define-regions-set_subdomain** 


- 2D shoebox geometry
- absorbing region
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep
- define region: using `set_subdomain`
- well defined materials interface

## Different options

There are a few different ways to "define regions", to be able to set
different physical properties. See the other demos for
`define-regions-`


## Screenshots
<img src="demo/define-regions-set_subdomain/results/screenshot_1.png" height="120">


Scripts:
- [`demo.py`](demo/define-regions-set_subdomain/demo.py)
- [`screenshots.py`](demo/define-regions-set_subdomain/screenshots.py)


[pvsm paraview file](demo/define-regions-set_subdomain/results/vis.pvsm)



