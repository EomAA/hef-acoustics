# This script is inteded to be run with pvbatch

from paraview.simple import *

# load state
LoadState(
    './results/vis.pvsm',
    LoadStateDataFileOptions='Search files under specified directory',
    DataDirectory='./results')

layout = GetLayoutByName("Layout #1")
SetActiveView(GetViewsInLayout(layout)[0])
Render()
layout.SetSize(1411, 821)
SaveScreenshot(
    './results/screenshot_1.png',
    layout,
    SaveAllViews=1,    
    ImageResolution=[1411, 821])

