# Illustrate admittance boundary condition (`set_subdomains`)

- **not working**
- 3D shoebox geometry
- absorbing patch
- wall admittance boundary condition
- point source
- resonance
- normal modes
- frequency sweep

## Known issues

This example is currently not working, because `set_subdomain` is not
implemented in 3D. See https://fenicsproject.org/qa/5630/subdomains-in-3d/
