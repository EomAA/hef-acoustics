# Normal modes in 2D rectangular domain (comparison with analytical solution)

- 2D shoebox geometry
- point source
- reflecting walls
- resonance
- nomal modes
- analytic solution: cosine functions (for specific frequencies)
- frequency sweep
- results normalization

## Remarks

The simulation code here is the same as the one for
`normal-modes-rectangular-room`. However, in this case some other code has been
included for the analytical solution in frequencies corresponding to
normal modes.

Comparison between numeric and analytical results require a
normalization. In this case the normalization was performed in
paraview. It is also possible to normalize results before writing them
to the data files, this can be found in [`point-source-3D`](demo/point-source-3D).