from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

width  = 1.1
height = 1.0

domain = Rectangle( Point(0.0,   0.0   ),
                    Point(width, height) )  # [m]

source_location = Point(0.7, 0.2)

H.mesh = generate_mesh(domain, 120)

H.init()

f_list = list(np.arange(200, 600, 10))

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_pa  = File(f'{output_dir}/pa.pvd')

def f_analytic_resonance(n1, n2) :
    """ Get frequency for mode (n1,n2) """
    # Pierce, A. D. Acoustics. Springer, 2019. Eq. (6.5.6)
    k = np.pi * ( (
        ( n1/width )**2 + ( n2/height )**2 )**0.5 )
    f = ( H.c*k ) / ( 2*np.pi )
    return f

class Analytic_solution(UserExpression) :
    n1 = None
    n2 = None
    def __init__(self, **kwargs) :
        super().__init__(**kwargs)
    def eval(self, value, x):
        # Pierce, A. D. Acoustics. Springer, 2019. Eq. (6.5.5)
        value[0] = (  np.cos(self.n1*np.pi*x[0]/width) 
                    * np.cos(self.n2*np.pi*x[1]/height) )
        value[1] = value[0]
    def value_shape(self):
        return (2,)
    
analytic_solution = Analytic_solution()

# Get all the frequencies for modes in the range defined with f_list
f_ar = dict()
n1   = 0
n2   = 0
while True :
    f = f_analytic_resonance(n1,n2)
    if f >= min(f_list) :
        if f <= max(f_list) :
            f_ar[f] = {
                "n1" : n1,
                "n2" : n2 }
            n1 += 1
        else :
            if n1 == 0 and n2 > 0 :
                break
            n1 = 0
            n2 += 1
    else :
        n1 += 1

# Include resonance frequencies in the frequency list to be simulated
f_list = sorted(f_list + [f for f in f_ar])

# Analytic solution
pa_c = Function(H.V, name="pa")

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    bc = PointSource(H.V, source_location, 1)

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))
    
    if H.f in f_ar :
        print(f"Mode for n1:{f_ar[H.f]['n1']}, n2:{f_ar[H.f]['n2']}")
        analytic_solution.n1 = f_ar[H.f]["n1"]
        analytic_solution.n2 = f_ar[H.f]["n2"]
        assign(pa_c,  interpolate(analytic_solution, H.V))
        fid_pa << (pa_c, float(H.f))

        
