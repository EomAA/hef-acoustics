#  Normal modes in 2D rectangular domain (comparison with analytical solution) 

directory name: **normal-modes-rectangular-room-analytic-comparison** 


- 2D shoebox geometry
- point source
- reflecting walls
- resonance
- nomal modes
- analytic solution: cosine functions (for specific frequencies)
- frequency sweep
- results normalization

## Remarks

The simulation code here is the same as the one for
`normal-modes-rectangular-room`. However, in this case some other code has been
included for the analytical solution in frequencies corresponding to
normal modes.

Comparison between numeric and analytical results require a
normalization. In this case the normalization was performed in
paraview. It is also possible to normalize results before writing them
to the data files, this can be found in [`point-source-3D`](demo/point-source-3D).

## Screenshots
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_1a.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_1b.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_2.png" height="120">
<img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/normal-modes-rectangular-room-analytic-comparison/demo.py)
- [`screenshots.py`](demo/normal-modes-rectangular-room-analytic-comparison/screenshots.py)


[pvsm paraview file](demo/normal-modes-rectangular-room-analytic-comparison/results/vis.pvsm)



