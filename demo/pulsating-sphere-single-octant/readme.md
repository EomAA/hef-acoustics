#  Pulsating sphere (single octant) 

directory name: **pulsating-sphere-single-octant** 


- **not working**
- 3D free space geometry
- single octant
- anechoic boundary condition
- fixed frequency
- pulsating sphere
- overriding `k_expression`
- local mesh refinement
- radiation

## Known issues

### The sphere is not smooth

Even when this script works better than `pulsating-sphere-simple`. In this
case since the surface of sphere is generated for the original mesh,
the refined sphere still have big flat surfaces.

The alternative seems to be to use `gmsh`, which have an option to de
define the mesh refinement for specific surfaces.


## Screenshots
<img src="demo/pulsating-sphere-single-octant/results/screenshot_1.png" height="120">
<img src="demo/pulsating-sphere-single-octant/results/screenshot_2.png" height="120">
<img src="demo/pulsating-sphere-single-octant/results/screenshot_3.png" height="120">


Scripts:
- [`demo.py`](demo/pulsating-sphere-single-octant/demo.py)
- [`screenshots.py`](demo/pulsating-sphere-single-octant/screenshots.py)


[pvsm paraview file](demo/pulsating-sphere-single-octant/results/vis.pvsm)



