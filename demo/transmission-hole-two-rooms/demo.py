from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

# Forcing velocity amplitude
U0 = 6.0

air = Rectangle(
    Point(-3.0, -1.5),
    Point( 2.5, 1.5) )

wall = Rectangle(
    Point(-0.5, -3.0),
    Point( 0.5,  3.0) )

hole = (
    Polygon([
        Point(-0.5,  0.05),
        Point(-0.5, -0.05),
        Point( 0.5, -0.15),
        Point( 0.5,  0.15) ])
    + Circle(
        Point(0.0, 0.1), 0.3, 20) )

domain = air - ( wall - hole )

H.mesh = generate_mesh(domain, 150)

# If this syntax is confusing see "Uneven frequency lists" in
# doc/3_FEniCS_and_Python.md
f_list = sorted(set(
    list(np.arange(1028, 1033, 0.5))
    + list(np.arange(1029, 1029.5, 0.05))
    + list(np.arange(1031, 1032.5, 0.05)) ))

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')

class Moving_wall(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 near(x[0], -3.0) )

moving_wall = Moving_wall()

H.U0_expression = Constant((U0, 0.0))
H.init(boundary_U0=moving_wall)

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b = H.Ab_assemble()

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c,   float(H.f))
