# Horn

- cylinder geometry
- axisymmetic
- resonance
- nomal modes
- frequency sweep
- absorbing region
- anechoic walls
- overriding `k_expression`
- wall velocity boundary condition
- radiation

## Additional dependencies

- shapely