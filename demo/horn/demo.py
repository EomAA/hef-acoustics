from dolfin import *
from mshr import *
import numpy as np
import pathlib
import sys
import shapely.geometry as sg

# import HEF-Acoustics main module
from hef import solver as H
from hef.utils import refine_mesh_on

# Speed of sound
H.c = 343 # [m/s]

H.axisymmetric = True

# Forcing velocity amplitude
U0 = 6.0

# Domain [m]
width  = 5
height = 3
horn_length = 2.74
horn_mouthpiece_radius = 0.01

# If this syntax is confusing see "Uneven frequency lists" in
# doc/3_FEniCS_and_Python.md
frequency_list = sorted(set(
    list(np.arange(280, 380, 10))
    + list(np.arange(289, 297, 1))
    + list(np.arange(350, 358, 1))
    + [1200] ))

output_dir = './results'

pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_m   = File(f'{output_dir}/mesh.pvd')
fid_p   = File(f'{output_dir}/p.pvd')
fid_k  = File(f'{output_dir}/k.pvd')

class Moving_wall(SubDomain):
    def inside(self, x, on_boundary):
        return ( on_boundary and
                 near(x[0], -horn_length) and
                 x[1] <= horn_mouthpiece_radius )

moving_wall = Moving_wall()

H.U0_expression = Constant((U0, 0.0))

class k_Expression(UserExpression):

    def __init__(self, x1_min=0.0, k_r=0.0, k_i=0.0, **kwargs) :
        super().__init__(**kwargs)
        self.k_r    = k_r
        self.k_i    = k_i
        self.x1_min = x1_min

    def eval_cell(self, value, x, cell):
        value[0] = self.k_r
        x0b = width/2 - 1
        x1b = height - 1
        # value[1] is k_i
        # There is possibly a better way to write this conditional
        if x[0] < 0 :
            if ( x[1] > self.x1_min and
                 ( abs(x[0]) > x0b or abs(x[1]) > x1b ) ) :
                value[1] = - 5*max(
                    [abs(x[0]) - x0b,
                     abs(x[1]) - x1b])**2
            else :
                value[1] = 0
        else :
            if abs(x[0]) > x0b or abs(x[1]) > x1b :
                value[1] = - 5*max(
                    [abs(x[0]) - x0b,
                     abs(x[1]) - x1b])**2
            else :
                value[1] = 0
    def value_shape(self):
        return (2,)

def horn_profile (x, B=0.02,  m=0.5, xinf=0.02) :
    # xinf: value for x where the horn have infinite slope
    y = B*(1/(xinf - x))**m
    if type(y) == complex :
        raise RuntimeError(f"""
Profile is not well defined for {x},
the calculated value is not real: {y}""")
    return  y

def generate_wall (coords, dx) :
    line = sg.LineString(coords)
    line_b = line.buffer(dx)
    return np.array(line_b.exterior.coords)

external_air = Rectangle(
    Point(-width/2, 0),
    Point( width/2, height) ) # [m]

horn_x0_list = sorted(set(
    list(np.arange(-horn_length, -0.7, 0.05))
    + list(np.arange(-0.7, -0.1, 0.01))
    + list(np.arange(-0.1, 0, 0.005))
    + [0] ))

horn_x1_list = [ horn_profile(x0) for x0 in horn_x0_list ]

horn_x1_list = [
    x1 + horn_mouthpiece_radius - min(horn_x1_list)
    for x1 in horn_x1_list ]

with open(f'{output_dir}/profile.csv','w') as fd :
    fd.write(f"x,y\n")
    for x,y in zip(horn_x0_list, horn_x1_list) :
        fd.write(f"{x:0.4f},{y:0.4f}\n")

# it is pretty common to have the points in the opposite order, to
# reverse them you can use `np.flipud`

horn_air_coords = np.flipud( list(zip(horn_x0_list, horn_x1_list))
         + [ (horn_x0_list[-1], 0),
             (horn_x0_list[0],  0) ] )

horn_air = Polygon( [ Point(x0, x1) for (x0, x1) in horn_air_coords ] )

horn_wall_coords = np.flipud( generate_wall(
    coords = list(zip(horn_x0_list, horn_x1_list)),
    dx = horn_mouthpiece_radius/10) )

horn_wall = Polygon( [ Point(x0, x1) for (x0, x1) in horn_wall_coords ] )

domain = external_air + horn_air - horn_wall

mesh = generate_mesh(domain, 120)

fid_m << mesh

mesh = refine_mesh_on([
    lambda x : x[0] < 0 and x[1] < horn_profile(x[0]),
    lambda x : x[0] < 0 and x[1] < horn_profile(x[0]),
    lambda x : ( (x[0]-0)**2 + (x[1]-horn_profile(0))**2 ) < 0.1 ],
    mesh = mesh,
    min_dx = 0.001 )

H.k_expression = k_Expression(
    # TODO: automate reading of this value
    x1_min = 0.15,
    degree=0 )

H.mesh = mesh

H.init(boundary_U0=moving_wall)

for H.f in frequency_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    A, b     = H.Ab_assemble()

    p_c      = Function(H.V, name="p")
    solve(A, p_c.vector(), b)
    p_r, p_i = p_c.split()
    k_r, k_i = H.k_c.split()

    fid_p << (p_c,   float(H.f))
    fid_k << (H.k_c, float(H.f))
