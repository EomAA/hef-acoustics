# Modes in 2D rectangular duct

- 2D infinite duct geometry
- absorbing layer
- reflecting walls
- overriding `k_expression`
- Dirichlet boundary condition
- frequency sweep

## Infinite length

The use of an absorbing layer in this case is to emulate the case of a
infinite duct.

## Cut-on frequency

See that the plane wave mode always propagates, but for higher order
modes the propagation ocurs only from some value of the frequency, a
cut-on frequency. When the mode is excited for lower values of the
frequency this mode decays exponentially, that is to say, it is an
evanescent mode.

There is a geometric explanation of this phenomena, as a bouncing
plane wave, in the upper a lower walls. This solution is equivalent to
the superposition of two plane waves, travelling at different
angles. You can see the geometry of this case in [this other demo](demo/two-plane-waves).
