# `duct-irises-traveling`



## Screenshots
<img src="demo/duct-irises-traveling/results/screenshot_1.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_2.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_3.png" height="120">
<img src="demo/duct-irises-traveling/results/screenshot_4.png" height="120">


Scripts:
- [`demo.py`](demo/duct-irises-traveling/demo.py)
- [`screenshots.py`](demo/duct-irises-traveling/screenshots.py)


[pvsm paraview file](demo/duct-irises-traveling/results/vis.pvsm)



