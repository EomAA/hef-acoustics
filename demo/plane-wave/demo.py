from dolfin import *
from mshr import *
import numpy as np
import sys
import pathlib
import sys

# import HEF-Acoustics main module
from hef import solver as H

domain = Rectangle( Point(0.0, 0.0),
                    Point(1.1, 1.0) )  # [m]

H.mesh = generate_mesh(domain, 100)

# angle [degrees]
theta = 30

analytic_solution = Expression(
    ("cos(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)",
     "sin(k*cos(theta*pi/180)*x[0]+k*sin(theta*pi/180)*x[1] - omega*t)"),
    pi = np.pi,
    theta = theta,
    k = 0,
    omega = 0,
    t = 1,
    degree = 2)

H.init()

f_list = np.arange(100,2500,100)

output_dir = './results'
pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
fid_p   = File(f'{output_dir}/p.pvd')
fid_pa  = File(f'{output_dir}/pa.pvd')

for H.f in f_list :

    print(f"Working on f={H.f:0.2f} Hz")

    H.update_frequency()

    analytic_solution.k     = H.k_expression.k_r
    analytic_solution.omega = H.omega

    bc = DirichletBC(
        H.V,
        analytic_solution,
        'on_boundary')

    A, b = H.Ab_assemble(bc)

    p_c = Function(H.V, name="p")
    solve(A, p_c.vector(), b)

    # The use of `H.f` here is discussed in
    # doc/4_Paraview.md#time-and-frequency
    fid_p << (p_c, float(H.f))

    # Save analitic solution
    pa_c = Function(H.V, name="pa")
    assign(pa_c,  interpolate(analytic_solution, H.V))
    fid_pa << (pa_c, float(H.f))
