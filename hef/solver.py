"""Solver for the Helmholtz equation

   Nabla^2 p + k^2 p = 0

where p, and k are complex

p = p_r + 1j * p_i
k = k_r + 1j * k_i

See that the Laplace operator does not have a minus sign, to be as
consistent as possible with notation in acoustics.

In this code

k_r = omega / c = 2 pi f / c
k_i = - alpha

where
  f     : frequency
  omega : angular frequency
  c     : speed of sound
  alpha : attenuation coefficient

"""

from dolfin import *
from mshr   import *
import numpy as np

mpi_comm = MPI.comm_world
mpi_rank = mpi_comm.Get_rank()

import hef

# Just for the user to see if the path for the `hef` module is correct
if mpi_rank == 0 :
    print(hef)

if hef.engine not in ["fenics", "dolfin", "dolfin_adjoint", "dolfinx", "firedrake"] :
    raise RuntimeError(f'Invalid engine: {hef.engine}')

if mpi_rank == 0 :
    print(f"INFO: Using engine `{hef.engine}`")

# Values "fenics" and "dolfin" are the same. No action is required.

if hef.engine == "dolfin_adjoint" :
    from dolfin_adjoint import *

if hef.engine == "dolfinx" :
    raise RuntimeError(f'Use of engine `{hef.engine}` is not yet implemented')

if hef.engine == "firedrake" :
    raise RuntimeError(f'Use of engine `{hef.engine}` is not yet implemented')


# After importing this module, override the following to define your
# own system. See the examples, they do just this.
# (((

# Frequency
f = 100  # [Hz]

# Speed of sound, you can locally modify this using k_r
c = 343  # [m/s]

# Equilibrium density
rho = 1.2  # [kg/m^3]

# Whether the domain has axial symmetry. In this case
# the symmetry axis is the bottom line, i.e.
# z = x[0]
# r = x[1]
axisymmetric = False

# A valid dolfin mesh, expected to be given
mesh = None

# Wave number, over the whole domain. You could define something
# similar for Y and U0.
class k_Expression(UserExpression):
    
    k_r = None
    k_i = None

    def __init__(self, **kwargs) :
        super().__init__(**kwargs)
        omega = 2.0*np.pi*f
        self.k_r = omega/c
        self.k_i = 0.0

    def eval(self, value, x):
        value[0] = self.k_r # = omega / c
        value[1] = self.k_i # = - alpha

    def value_shape(self):
        return (2,)

# The use of degree=0 is beacuse of the discussion at
# https://fenicsproject.org/qa/9425/setting-discontinuous-function-correctly-function-variable/
k_expression  = k_Expression(degree=0)
Y_expression  = Constant((0.0, 0.0))
U0_expression = Constant((0.0, 0.0))

# )))

def init(boundary_Y=None, boundary_U0=None, V_degree=2) :

    # The values for all this variables can be changed in this
    # function.
    # Is this a bad practice?
    global \
        E_r, E_i, E_c, V, V0, \
        p_r, p_i, \
        v_r, v_i, \
        k_c, k_r, k_i, \
        Y_c, Y_r, Y_i, \
        U0_c, U0_r, U0_i, \
        a_r, a_i, L_r, L_i, \
        subdomains, \
        ds, dx, \
        init_run

    E_r = FiniteElement('CG', mesh.ufl_cell(), V_degree)
    E_i = FiniteElement('CG', mesh.ufl_cell(), V_degree)
    E_c = E_r * E_i
    V   = FunctionSpace(mesh, E_c)

    kE_r = FiniteElement('DG', mesh.ufl_cell(), 0)
    kE_i = FiniteElement('DG', mesh.ufl_cell(), 0)
    kE_c = kE_r * kE_i
    V0   = FunctionSpace(mesh, kE_c)

    p_r, p_i   = TrialFunction(V)
    v_r, v_i   = TestFunction(V)
    k_c        = Function(V0, name="k")
    Y_c        = Function(V,  name="Y")
    U0_c       = Function(V,  name="U0")
    k_r,  k_i  =  k_c.split()
    Y_r,  Y_i  =  Y_c.split()
    U0_r, U0_i = U0_c.split()

    if ( axisymmetric and mesh.topology().dim() != 2 ) :
        raise Exception('To use axisymmetric=True, you must provide a 2D mesh.')

    subdomains = MeshFunction("size_t",
                              mesh,
                              mesh.topology().dim() - 1)
    subdomains.rename("subdomains", "subdomains")
    subdomains.set_all(0)
    if boundary_Y is not None :
        # Robin boundary condition
        boundary_Y.mark(subdomains, 1)
    if boundary_U0 is not None :
        # Neumann boundary condition
        boundary_U0.mark(subdomains, 2)

    dx = Measure('dx', domain=mesh, subdomain_data=subdomains)
    ds = Measure('ds', domain=mesh, subdomain_data=subdomains)

    cartesian = True if not axisymmetric else False

    # Weak form of the Helmholtz equation
    if cartesian :

        a_r = (
            - inner(nabla_grad(p_r), nabla_grad(v_r))*dx
            + inner(nabla_grad(p_i), nabla_grad(v_i))*dx
            + (k_r**2 - k_i**2)*inner(p_r, v_r)*dx
            - (k_r**2 - k_i**2)*inner(p_i, v_i)*dx
            - (2*k_r*k_i      )*inner(p_r, v_i)*dx
            - (2*k_r*k_i      )*inner(p_i, v_r)*dx
            + rho*c*(
                + k_r*Y_r*inner(p_r, v_i)*ds(1)
                + k_r*Y_r*inner(p_i, v_r)*ds(1)
                + k_r*Y_i*inner(p_r, v_r)*ds(1)
                - k_r*Y_i*inner(p_i, v_i)*ds(1) ) )

        a_i = (
            - inner(nabla_grad(p_r), nabla_grad(v_i))*dx
            - inner(nabla_grad(p_i), nabla_grad(v_r))*dx
            + (k_r**2 - k_i**2)*inner(p_r, v_i)*dx
            + (k_r**2 - k_i**2)*inner(p_i, v_r)*dx
            + (2*k_r*k_i      )*inner(p_r, v_r)*dx
            - (2*k_r*k_i      )*inner(p_i, v_i)*dx
            + rho*c*(
                - k_r*Y_r*inner(p_r, v_r)*ds(1)
                + k_r*Y_r*inner(p_i, v_i)*ds(1)
                + k_r*Y_i*inner(p_r, v_i)*ds(1)
                + k_r*Y_i*inner(p_i, v_r)*ds(1) ) )

        L_r = (
            + rho*c*(
                - k_r*U0_r*v_i*ds(2)
                - k_r*U0_i*v_r*ds(2) ) )

        L_i = (
            + rho*c*(
                + k_r*U0_r*v_r*ds(2)
                - k_r*U0_i*v_i*ds(2) ) )

    elif axisymmetric :

        x = SpatialCoordinate(mesh)

        a_r = (
            - inner(nabla_grad(p_r), nabla_grad(v_r))*x[1]*dx
            + inner(nabla_grad(p_i), nabla_grad(v_i))*x[1]*dx
            + (k_r**2 - k_i**2)*inner(p_r, v_r)*x[1]*dx
            - (k_r**2 - k_i**2)*inner(p_i, v_i)*x[1]*dx
            - (2*k_r*k_i      )*inner(p_r, v_i)*x[1]*dx
            - (2*k_r*k_i      )*inner(p_i, v_r)*x[1]*dx
            + rho*c*(
                + k_r*Y_r*inner(p_r, v_i)*x[1]*ds(1)
                + k_r*Y_r*inner(p_i, v_r)*x[1]*ds(1)
                + k_r*Y_i*inner(p_r, v_r)*x[1]*ds(1)
                - k_r*Y_i*inner(p_i, v_i)*x[1]*ds(1) ) )

        a_i = (
            - inner(nabla_grad(p_r), nabla_grad(v_i))*x[1]*dx
            - inner(nabla_grad(p_i), nabla_grad(v_r))*x[1]*dx
            + (k_r**2 - k_i**2)*inner(p_r, v_i)*x[1]*dx
            + (k_r**2 - k_i**2)*inner(p_i, v_r)*x[1]*dx
            + (2*k_r*k_i      )*inner(p_r, v_r)*x[1]*dx
            - (2*k_r*k_i      )*inner(p_i, v_i)*x[1]*dx
            + rho*c*(
                - k_r*Y_r*inner(p_r, v_r)*x[1]*ds(1)
                + k_r*Y_r*inner(p_i, v_i)*x[1]*ds(1)
                + k_r*Y_i*inner(p_r, v_i)*x[1]*ds(1)
                + k_r*Y_i*inner(p_i, v_r)*x[1]*ds(1) ) )

        L_r = (
            + rho*c*(
                - k_r*U0_r*v_i*x[1]*ds(2)
                - k_r*U0_i*v_r*x[1]*ds(2) ) )

        L_i = (
            + rho*c*(
                + k_r*U0_r*v_r*x[1]*ds(2)
                - k_r*U0_i*v_i*x[1]*ds(2) ) )

    update_frequency()
    init_run = True

def update_frequency(f_local = None) :
    """Update frequency, and other data depending on it.
    """

    # The values for all this variables can be changed in this
    # function
    global f, omega, \
        k_expression, Y_expression, U0_expression, \
        k_c, Y_c, U0_c

    if f_local is not None :
        f = f_local
    else :
        # use the current global value of `f`
        pass

    omega = 2.0*np.pi*f

    k_expression.k_r = omega/c
    k_expression.f   = f
    Y_expression.f   = f
    U0_expression.f  = f

    # The use of `interpolate` (rather than `project`) is preferred,
    # because it assigns the right values, while `project` has
    # wrinkles. However, interpolate is a little slower.
    assign(k_c,  interpolate( k_expression, V0))
    assign(Y_c,  interpolate( Y_expression, V ))
    assign(U0_c, interpolate(U0_expression, V ))

def Ab_assemble(bcs=None) :

    if not init_run :
        raise Exception('Please use the init function before calling this.')

    # How can this be correct? We build the components of a and L
    # separated. But, here we add then add them together. However, it
    # seems to work as expected. Why?
    A = assemble(a_r + a_i)
    b = assemble(L_r + L_i)

    if bcs is not None :
        # Apply provided boundary conditions
        for bc in np.atleast_1d(bcs) :

            bc.apply(b)
            bc.apply(A)

            # See issue #3

    return A, b
