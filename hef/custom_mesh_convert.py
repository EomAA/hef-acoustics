import sys
import meshio
import pathlib
import numpy as np

infile = sys.argv[1] if len(sys.argv) >= 2 else "usr_occ.msh"
outdir = sys.argv[2] if len(sys.argv) >= 3 else "."

def main() :

    global infile, outdir
    
    infile = pathlib.Path(infile)
    outdir = pathlib.Path(outdir)
    
    assert outdir.is_dir(), "ERROR: {outdir} is not a directory"
    assert infile.is_file(), "ERROR: {infile} not found"

    msh = meshio.read(infile)

    triangle_cells = []
    tetra_cells = []
    for cell in msh.cells:
        if cell.type == "triangle":
            triangle_cells = cell.data
        elif  cell.type == "tetra":
            tetra_cells = cell.data

    triangle_data = []
    tetra_data = []
    for key in msh.cell_data_dict["gmsh:physical"].keys():
        if key == "triangle":
            triangle_data = msh.cell_data_dict["gmsh:physical"][key]
        elif key == "tetra":
            tetra_data = msh.cell_data_dict["gmsh:physical"][key]

    triangle_reduced_index = []
    for cell in triangle_cells :
        for point in cell :
            if point not in triangle_reduced_index :
                triangle_reduced_index.append(point)

    triangle_reduced_mesh = []
    for point in triangle_reduced_index :
        triangle_reduced_mesh.append(
            msh.points[point])
    triangle_reduced_mesh = np.array(triangle_reduced_mesh)

    triangle_reduced_cells = []
    for cell in triangle_cells :
        cell_new = []
        for point in cell :
            cell_new.append(triangle_reduced_index.index(point))
        triangle_reduced_cells.append(cell_new)
    triangle_reduced_cells = np.array(triangle_reduced_cells)

    if len(tetra_cells) > 0 :
        tetra_mesh = meshio.Mesh(
            points = msh.points,
            cells = {"tetra": tetra_cells},
            cell_data = {"materials":[tetra_data]})
    else :
        tetra_mesh = None

    if len(triangle_cells) > 0 :
        triangle_mesh = meshio.Mesh(
            points = msh.points,
            cells = [("triangle", triangle_cells)],
            cell_data = {"surfaces":[triangle_data]})
    else :
        triangle_mesh = None

    if len(triangle_reduced_cells) > 0 :
        triangle_reduced_mesh_mesh = meshio.Mesh(
            points = triangle_reduced_mesh,
            cells = [("triangle", triangle_reduced_cells)],
            cell_data = {"surfaces":[triangle_data]})
    else :
        triangle_reduced_mesh_mesh = None

    if tetra_mesh is not None :
        meshio.write(
            outdir.joinpath(infile.with_suffix(".xdmf").name),
            tetra_mesh)

    if triangle_mesh is not None :
        meshio.write(
            outdir.joinpath(infile.with_suffix(".mf.xdmf").name),
            triangle_mesh)

    if triangle_reduced_mesh_mesh is not None :
        meshio.write(
            outdir.joinpath(infile.with_suffix(".reduced.mf.xdmf").name),
            triangle_reduced_mesh_mesh)
        meshio.write(
            outdir.joinpath(infile.with_suffix(".vtu").name),
            triangle_reduced_mesh_mesh)

if __name__ == '__main__':
    main()
