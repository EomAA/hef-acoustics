from dolfin import *

# import HEF-Acoustics main module
from hef import solver as H

# TODO: give the option to get values for `omega`, `rho` y and `mesh`
# as parameters, they could be different than the ones we are
# currently using.

def calculate(p_r, p_i) :

    if p_r.geometric_dimension() == 2 :
        #TODO: Check if `as_matrix` is a better choice here. The idea
        #is to have a data structure of 4 components, rather than 9.
        u_t_e = as_tensor((
            Constant(-1/(H.omega*H.rho)) * grad(p_i),
            Constant( 1/(H.omega*H.rho)) * grad(p_r)))

    elif p_r.geometric_dimension() == 3 :
        u_t_e = as_tensor((
            Constant(-1/(H.omega*H.rho)) * grad(p_i),
            Constant( 1/(H.omega*H.rho)) * grad(p_r),
            (Constant(0), Constant(0), Constant(0))    ))

    else :
        raise RuntimeError("This is implemented for 2D and 3D only.")

    V_t = TensorFunctionSpace(H.mesh, 'CG', degree=2)
    u_c = project(u_t_e, V_t)
    u_c.rename("u", "u")

    return u_c

def get_magnitude(u_c) :

    #TODO: Implement
    #
    # Accoding to the discussion in the documentation.
    #
    # Check if the phases of the componets are "close" to one another.
    # If they do
    #    return \sqrt{|u_x|^2 + |u_y|^2 + |u_z|^2}
    # If they don't
    #    return NAN

    ...
