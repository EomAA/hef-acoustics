# Convergence Analysis

Some of the offered demos in this repository are convergence
tests. Because of the specific nature of this demos, they are in a
[separate directory](demo/convergence-tests), but other than that they
are just like the others.

This document was written using the results corresponding to [this
state of the
code](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6). You
can find discussion on convergence for previous states of the code in
the following links:

- [Presentation for the ASA meeting fall
  2021](https://gitlab.com/cfd-pizca/hef-acoustics/-/blob/master/doc/details/slides_ASA_meeting_fall21.pdf)
- [Convergence tests summary, 15 nov
  2021](https://gitlab.com/cfd-pizca/hef-acoustics/-/blob/4ba0c47b521c21978a367c867df48d60980b1d84/convergence_analysis.md)

## Error estimation

The percent error $`E_p`$ is computed as 

```math
E_p =  100 \cdot \frac{ | p_N - p_A | }{ | p_A | }
```

where
- $`p_N`$ : Numeric result
- $`p_A`$ : Analytic solution
- the norm is $`L^2`$ (computed with FEniCS `errornorm`)

This error is expected to decay exponentially with mesh refinement, as

```math
E_p = K \cdot N^{-\eta}
```

with
- $`\eta`$ : Convergence rate
- $`N`$ : Cell diameters per wavelength
- $`K`$ : Some constant

Then, we perform a set of simulations. On the one hand, we change some
configuration of the system. For instance, for the plane wave case
we change the angle of propagation; for the systems with normal modes
we use the corresponding list of frequencies. On the other hand, for
each of these configurations we change logarithmically the value of
$`N`$, refinig the mesh.

After that, for each test, we make averages for the values of $`E_p`$
corresponding to the same values of $`N`$. With these series we make a
linear regression using $`log(N)`$ as the independent variable, and
$`log(E_p)`$, as the dependent variable. With this regression we get
two parameters:
- An estimation for the convergence rate $`\eta`$.
- An estimation for the value of $`N`$ needed to get $`E_p=1`$, that
  is 1% error. We call this value $`N_1`$.
  
The following figures illustrate this analysis, for the four cases
where this test have been performed. The linear regression is shown as
a "dash" gray line.

| <img src="https://gitlab.com/cfd-pizca/hef-acoustics/-/raw/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/plane-wave/results/screenshot_1.png" height="360"> |
| --- |
| [plane wave](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/plane-wave) |

| <img src="https://gitlab.com/cfd-pizca/hef-acoustics/-/raw/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/pulsating-cylinder-cartesian/results/screenshot_1.png" height="360"> |
| --- |
| [pulsating cylinder](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/pulsating-cylinder-cartesian) |

| <img src="https://gitlab.com/cfd-pizca/hef-acoustics/-/raw/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/normal-modes-rectangular-room/results/screenshot_1.png" height="360"> |
| --- |
| [normal modes in a rectangular room](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/normal-modes-rectangular-room) |

| <img src="https://gitlab.com/cfd-pizca/hef-acoustics/-/raw/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/normal-modes-axisymmetric-simple/results/screenshot_1.png" height="360"> |
| --- |
| [normal mode in a simple axisymmetric domain](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/normal-modes-axisymmetric-simple) |

The corresponding values for $`\eta`$ and $`N_1`$ are

| case                                        | $`\eta`$ | $`N_1`$ |
| ----                                        | -----    | ----    |
| plane wave                                  | 4.33     | 6.81    |
| pulsating cylinder                          | 2.47     | 5.61    |
| normal modes in a rectangular room          | 3.65     | 7.72    |
| normal mode in a simple axisymmetric domain | 3.26     | 3.1     |

With this data, we can see that the convergence depend a little on the
specifics of the system. However, it is possible to conclude that:
- The convergence rate for all cases is above 2, which would
  correspond to a second order method.
- With a value of $`N`$ greater that 7 good numerical results can be
  expected.
  
Further literature review is needed to check how these numbers compare
with other FEM implementations for acoustics systems.
  
## Additional remarks

### Degree of the function space

The current state of the code use, by default, a "Lagrange" function
space of degree 2. Previous versions of the code, and currently some
3D examples, use degree 1, which results in much larger values for
$`N_1`$ as you can see in the links listed above.

In the code, this parameter is controlled with the argument `V_degree`
of the function `init`, for details see
[here](doc/3_FEniCS_and_Python.md), and an explicit usage of this
argument [here](demo/pulsating-sphere-gmsh).

### Additional tests

There is [another
test](https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/f650bd1e6d66b282e50b372d72a5a1945346eac6/demo/convergence-tests/plane-wave-fixed-mesh),
where the analysis and the results are a little different.

There are other tests, planed to be implemented in the future,
currently those have empty scripts.
