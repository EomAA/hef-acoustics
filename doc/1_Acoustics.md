# List of examples for acoustics systems

You can find a list of examples for acoustics systems [here](doc/details/list_of_acoustics_systems.md).

You can find a list of the examples implementig each of the available
boundary contitions
[here](doc/details/list_of_implemented_boundary_conditions.md).

# Time-harmonic convention

In this repository, we are considering
```math
\tilde{p}(\vec{x},t) = p(\vec{x}) e^{i\omega t}
```

Even when the choice of sign (inside the exponential function) has no
effect for the obtention of the Helmholtz equation in the case where
$`k`$ is real, it does matter for the complex case, and for the
boundary conditions.

For some details on the actual version of the Helmholtz equation
considered in this repository, see
[here](doc/details/Helmholtz_equation.pdf).

# Point source and normalization

The implementation of a "point source" has some problems (see issue
#12). A manifestation of this problem is that the amplitude of the results cannot
be predicted very well. See for example,

<img src="demo/point-source-2D/results/screenshot_1.png" height="120">
<img src="demo/point-source-3D/results/screenshot_1.png" height="120">
<img src="demo/normal-modes-rectangular-room/results/screenshot_1.png" height="120">

However, the profile of the numeric results can be independent on the
source, specially in resonance. Then, in the case an analytic solution
is available, in order to make a comparison a normalization is needed.

For the presented examples, this normalization has been made in two
different ways:
- in the simulation script, see [point-source-3D](demo/point-source-3D)
- in paraview, see [normal-modes-rectangular-room-analytic-comparison](demo/normal-modes-rectangular-room-analytic-comparison)

## Dependence on position

Please see that if you move the position on the point source the
numeric solution in resonance can "flip". The point source seems to
force the numeric solution to have a positive value of the real part
of $`p`$ at the possition where the point source is.

This is specially anoying when trying to measure error between numeric
and analytic solutions. The percent_error masurement takes values near
200.

That is why the position of the point sources has been taken to be
very close to a place where the analytic solution is known to have a
value close to 1.

# Multiple scales

TODO: write a little discussion on this point for two different cases:
- multiple scales in length, and
- multiple scales in pressure amplitude.

(mention the log plots of some examples)

# Analytic solutions

TODO: Write a little discussion about this.

- Some examples correspond to acoustic system where it's easy to have
  an analytic solution
- Some implementations of this analytic solution are in the simulation
  scripts
- Some are included in postprocessing, in paraview
- some care must be taken to check if the analytic solution was
  obtained using an harmonic time dependence as $`exp(-i \omega t)`$ or
  $`exp(i\omega t)`$.

# Presentation slides

- [Acoustical Society of America 181st Meeting, Seattle, WA, 29
November-3 December 2021](doc/details/slides_ASA_meeting_fall21.pdf).
- [MexSIAM 2022 (in spanish)](doc/details/MexSIAM-2022.pdf)
