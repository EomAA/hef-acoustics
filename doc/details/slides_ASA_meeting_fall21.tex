% Created 2022-04-18 Mon 17:52
% Intended LaTeX compiler: pdflatex
\documentclass[presentation, t, aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{pgfpages}
\usepackage{physics}
\newcommand{\D}{\Omega}
\newcommand{\B}{{\partial\Omega}}
\newcommand{\BY}{{\partial\Omega_Y}}
\newcommand{\BU}{{\partial\Omega_U}}
\usetheme{Szeged}
\author{Roberto Velasco-Segura and Cristian U. Martínez-Lule \\
\vspace{20pt}\small{Acoustics and Vibrations Group,} \\
\small{The Institute for Applied Sciences and Technology, ICAT} \\
\small{UNAM, México.} \\
\vspace{20pt}\footnotesize{181st Meeting of the Acoustical Society of America, Seattle, Washington, November 30, 2021.}}
\date{\today}
\title{A Helmholtz Equation Implementation for FEM, Based on FEniCS Project}
\defbeamertemplate*{title page}{customized}[1][]
{
\usebeamerfont{title}\usebeamercolor[blue]{title}\inserttitle\par\vspace{30pt}
\usebeamerfont{subtitle}\usebeamercolor[black]{subtitle}\insertsubtitle\par
\bigskip
\usebeamerfont{author}\usebeamercolor[black]{author}\insertauthor\par
\usebeamerfont{institute}\usebeamercolor[black]{institute}\insertinstitute\par
}
\date{}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
\insertshorttitle\hfill%
\insertframenumber\,/\,\inserttotalframenumber}
\usepackage{multimedia}
\setbeamerfont{title}{size=\Huge}
\setbeamertemplate{navigation symbols}{}
\setbeameroption{show notes on second screen}
\setbeamercolor{note page}{bg=gray, fg=white}
\setbeamertemplate{note page}{%
\begin{note box}
\addtolength{\parskip}{\baselineskip}
\usebeamercolor[black]{note page}%
\setbeamercolor{itemize/enumerate body}{fg=black}%
\setbeamercolor{itemize/enumerate subbody}{fg=black}%
\setbeamercolor{itemize/enumerate subsubbody}{fg=black}%
\setbeamercolor{itemize/enumerate item}{fg=black}%
\setbeamercolor{itemize/enumerate subitem}{fg=black}%
\setbeamercolor{itemize/enumerate subsubitem}{fg=black}%
% \hline \hline \hline
\insertnote \vfill  \insertframenumber \vspace{10pt}
\end{note box}}
\hypersetup{
 linkcolor=blue,
 pdfauthor={Roberto Velasco-Segura and Cristian U. Martínez-Lule \\
\vspace{20pt}\small{Acoustics and Vibrations Group,} \\
\small{The Institute for Applied Sciences and Technology, ICAT} \\
\small{UNAM, México.} \\
\vspace{20pt}\footnotesize{181st Meeting of the Acoustical Society of America, Seattle, Washington, November 30, 2021.}},
 pdftitle={A Helmholtz Equation Implementation for FEM, Based on FEniCS Project},
 colorlinks=true,
 urlcolor=blue,
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.4.6)}, 
 pdflang={English}}
\begin{document}

\begin{frame}[noframenumbering,plain]
\vspace{20pt}
\titlepage
\vfill
\end{frame}

\note{\label{org0afcd8c}
Good morning.

My name is Roberto Velasco-Segura.

Thank you so much for the oportunity to be here.

I'm going to talk about this project, which is a Helmholtz
equation implementation for the Finite Element Method, based on
FEniCS.}

\begin{frame}[label={sec:orgbcf68db}]{DISCLAIMER}
Please note that this slides describe an old state of the code,
corresponding to this URL:

\url{https://gitlab.com/cfd-pizca/hef-acoustics/-/tree/ASA\_meeting\_fall21}

In the current state of the code, some of the examples are
different, and some of the results of the tests are a little
better.
\end{frame}

\begin{frame}[label={sec:org9c0323a}]{Code repository: HEF-Acoustics}
\url{https://gitlab.com/cfd-pizca/hef-acoustics}

\begin{columns}
\begin{column}{0.60\columnwidth}
\vspace{15pt}

Based on:
\begin{itemize}
\item \raisebox{-0.1in}{
\includegraphics[width=0.4in]{images/fenics.png}
}
FEniCS Project

\item \raisebox{-0.1in}{
\includegraphics[width=0.4in]{images/paraview.png}
}
Paraview

\item \raisebox{-0.1in}{
\includegraphics[width=0.4in]{images/gitlab.png}
}
Gitlab
\end{itemize}
\end{column}

\begin{column}{0.40\columnwidth}
\begin{columns}
\begin{column}{1\columnwidth}
\begin{block}{Contents of this presentation}
\vspace{20pt}
\tableofcontents
\end{block}
\end{column}
\end{columns}
\end{column}
\end{columns}

\note{\label{org1946453}
Most of the postprocessing and visualization is made in paraview, the
code is hosted in gitlab, and (here) is the URL.

There you can find the names of some of the people that have
contributed to this project.

The proposed name for this code is "HEF-Acoustics".

In this presentation I'm going to describe a few details of the
Finite Element Method, mainly the weak form and the boundary
conditions.

After that, I will present some numerical results to validate the code.

After that, some examples:
\vspace{-15pt}
\begin{itemize}
\item On the one hand, examples for fundamental, simple, acoustic systems.
\item On the other hand, some more elaborated examples, which we have
been using for original research.
\end{itemize}

Finally, I will mention some conclusion remarks.}
\end{frame}

\section{Finite Element Method}
\label{sec:org431d037}

\begin{frame}[label={sec:orgcb41d72}]{\label{org3eb8a1d}}
\vspace{-45pt}

\begin{columns}
\begin{column}{0.45\columnwidth}
\begin{block}{Helmholtz equation}
\begin{align}
\nabla^2 p + k^2 p = 0
\end{align}
where
\begin{align}
p &= p_r + i p_i \qquad \text{pressure} \\
k &= k_r + i k_i \qquad \text{wave number}
\end{align}
\end{block}
\end{column}

\begin{column}{0.55\columnwidth}
\begin{block}{Boundary conditions}
\begin{itemize}
\item Totally reflective (Neumann, natural)
\vspace{-10pt}
\begin{align}
\pdv{p}{\hat{n}} = 0
\end{align}

\item Dirichlet 
\vspace{-10pt}
\begin{align}
p= P_0
\end{align}

\item Non null admitance (Robin) 
\vspace{-10pt}
\begin{align}
\label{eq:bc_admitance}
\pdv{p}{\hat{n}} =-i \omega \rho_0 Y p
\end{align}

\item Vibrating wall (Neumann) 
\vspace{-10pt}
\begin{align}
\label{eq:bc_vib}
\pdv{p}{\hat{n}} = -i \omega \rho_0  U_0 
\end{align}
\end{itemize}
\end{block}
\end{column}
\end{columns}

\note{\label{org0856cba}
(Here) we have the Helmholtz equation, which describes stationary
states, including propagation.

A first approach for numeric implementation of this equation is to
take the pressure \(p\) and the wave number \(k\) to be real
numbers. However, to describe many of the relevant features of the
acoustic systems these variables need to be considered complex
numbers. As you can see, (here). I will return to this point in the
next slide.

The types of boundary conditions considered in this code are the
following:
\vspace{-15pt}

\begin{itemize}
\item A natural boundary condition for the Finite Element
Method. Which corresponds to a totally reflective wall, because
it is a null derivative of the pressure amplitude, with respect
to a coordinate transversal to the boundary.

\item We can also set the value of the pressure for some region, which is a
Dirichlet boundary condition.

\item As third option, we can have the mentioned derivative to be
proportional to the field \(p\) itself. With that we get a
partially reflective wall, with a coefficient related to the
admitance.

\item Finally, we can set a fixed value for this derivative, and have a
situation corresponding to a vibrating wall.
\end{itemize}}
\end{frame}

\begin{frame}[label={sec:org45e702a}]{Weak form (real part)}
\(a(p,v) = L(v)\) 

\begin{align}
a_r &=
 - \int_\D   \nabla v_r \cdot \nabla p_r              \ \dd v 
 - \int_\D   \nabla v_i \cdot \nabla p_i              \ \dd v \\
&+ \int_\D   ((k_r^2 - k_i^2)p_r - 2 k_r k_i p_i)v_r  \ \dd v 
 + \int_\D   ((k_r^2 - k_i^2)p_i - 2 k_r k_i p_r)v_i  \ \dd v \\
&+ \rho_0 c
   \bigg(
          \int_\BY  k_r v_r(Y_i p_r + Y_r p_i)        \ \dd s 
        - \int_\BY  k_r v_i(Y_r p_r - Y_i p_i)        \ \dd s 
   \bigg) \\
L_r &= 
   \rho_0 c
   \bigg(
 - \int_\BU  k_r v_r U_{0,i}                          \ \dd s 
 + \int_\BU  k_r v_i U_{0,r}                          \ \dd s
   \bigg) 
\end{align}

\note{\label{orgedcfe88}
(Here) we have the real part of the weak form for the Helmholtz
equation. See that the real and imaginary parts of the variables
are operated explicitly. This is because FEniCS doesn't currently
support complex numbers. Then, every complex variable is taken to
be a vector, of two components, which are are operated as real
scalar numbers.

We obtain this weak form with a standard procedure for the Finite
Element Method:
\begin{itemize}
\item take the product with a test function,
\item integrate over the simulation domain,
\item make use of the Gauss theorem,
\item get some surface integrals, where we apply the boundary
conditions, mentioned before.
\end{itemize}}
\end{frame}

\begin{frame}[label={sec:orgce18e0b}]{\label{orgb632f0d}}
\vspace{-35pt}

\begin{columns}
\begin{column}{0.30\columnwidth}
\begin{block}{Axisymmetric case}
Just express the integrals in cylindrical coordinates. 
\end{block}
\end{column}

\begin{column}{0.70\columnwidth}
\begin{block}{Anechoic walls and PML}
\begin{itemize}
\item PML is not yet implemented.
\item Anechoic walls are obtained using the imaginary part of the wave
number, with quadratic increase.

\vspace{20pt}

\includegraphics[width=0.85\textwidth]{images/pulsating-cylinder-cartesian-2.png}
\end{itemize}
\end{block}
\end{column}
\end{columns}

\note{\label{orgee9117e}
For the axisymmetric version of the code, we only express the
differential operators, and the integrals in cylindrical
coordinates, and remove derivatives with respect to the angular
coordinate, assuming nothing depends on this coordinate.

\noindent\rule{\textwidth}{0.5pt}

To have non-reflective walls, a formal Perfectly Matched Layer
implementation (PML) would be desirable, but currently it is not
implemented. We are using, with good results, the imaginary part
of the wave number, increasing it quadratically over a wide layer.
As you can see, (here), and (here).

We think this could be equivalent to a PML, but further check is
still needed.}
\end{frame}

\section{Validation}
\label{sec:orgd6696b6}

\begin{frame}[label={sec:org8910ecd}]{Comparisson between numeric results an analytic solutions}
\vspace{-20pt}

\begin{columns}
\begin{column}{0.40\columnwidth}
\begin{block}{Percent error}
\begin{align*}
E_p =  100 \cdot \frac{ | p_N - p_A | }{ | p_A | }
\end{align*}
\begin{itemize}
\item \(p_N\) : Numeric result
\item \(p_A\) : Analytic solution
\item norm is \(L^2\)
\end{itemize}
\end{block}
\end{column}

\begin{column}{0.60\columnwidth}
\begin{block}{Convergence rate}
\begin{align*}
E_p = K \cdot N^{-\eta}
\end{align*}
\begin{itemize}
\item \(\eta\) : Convergence rate
\item \(N\) : Cell diameters per wavelength
\item \(N_1\) : \(N\) for \(E_p\) in the order of 1\%
\item \(K\) : Some constant
\end{itemize}
\end{block}
\end{column}
\end{columns}

\vspace{5pt}

\fbox{\parbox{\textwidth}{
\hspace{20pt} 4 convergence tests were performed, but only 2 are presented here.
}}


\note{\label{org6cc33b8}
To validate the code we simulated some systems where analytic
solutions are available.

Then, we can evaluate the difference between numeric and analytic
results, take a L2 norm, and normalize to get a "percent error".

As with other numeric methods, the percent error is expected to
decay exponentially with mesh refinement.

As a measure of the refinement we use the "amount of cell
diameters in a wavelength" \(N\).

Then, we measure:
\begin{itemize}
\item the exponent \(\eta\) of this decay, and
\item the value \(N_1\), for which the percent error is in the order of
1\%.
\end{itemize}

4 convergence tests were performed, but only 2 are presented
here. The other two, and some further details can be found in the
repository.}
\end{frame}

\begin{frame}[label={sec:org7057910}]{Oblique plane wave propagation}
\vspace{-20pt}

\begin{columns}
\begin{column}{0.25\columnwidth}
\begin{block}{Pressure field}
\begin{center}
\includegraphics[width=.9\linewidth]{images/E-imgp_2.pdf}
\end{center}

\begin{itemize}
\item \(\eta = 1.99\)
\item \(N_1 \approx 60\)
\end{itemize}
\end{block}
\end{column}

\begin{column}{0.80\columnwidth}
\begin{block}{Convergence rate}
\begin{center}
\includegraphics[width=.9\linewidth]{images/E-imgp_1.pdf}
\end{center}
\end{block}
\end{column}
\end{columns}

\note{\label{org075d05e}
Here we see results from a set of simulations for plane waves, at
different angles.

We use a Dirichlet boundary condition, equal to the analytic
solution, over the whole boundary.

The different lines (here) correspond to the refinement of the
simulations for different angles.

As the mesh refines we see, as expected, a reduction on the
"Percent error", to the point that we have 1\% error for \(N\) close
to 60.

We see a straight gray line corresponding to \(N\) to the power
minus 1.99, which we think describes the global tendency of the
data.

The value 1.99 is obtained taking averages and using a linear
regression over the logarithms of the data.

Then, we say the convergence rate \(\eta\) is 1.99, almost 2, which
would correspond to a formal second order method.}
\end{frame}


\begin{frame}[label={sec:org845eebe}]{Normal modes in simple axisymmetric enclosure}
\vspace{-20pt}

\begin{columns}
\begin{column}{0.25\columnwidth}
\begin{block}{Pressure field}
\begin{center}
\includegraphics[width=0.3\textwidth]{images/C-imgp_2.pdf}
\includegraphics[width=0.4\textwidth]{images/normal-modes-axisymmetric-simple-analytic-comparison-1.pdf}
\end{center}

\begin{itemize}
\item \(\eta = 1.65\)
\item \(N_1 \approx 25\)
\end{itemize}
\end{block}
\end{column}

\begin{column}{0.80\columnwidth}
\begin{block}{Convergence rate}
\begin{center}
\includegraphics[width=.9\linewidth]{images/C-imgp_1.pdf}
\end{center}
\end{block}
\end{column}
\end{columns}

\note{\label{orgf148293}
Here is another convergence test for a similar case. This is a simple
axisymmetric enclosure where we know normal modes correspond to
Bessel functions.

The geometry is similar to a Petri dish.

The different lines correspond to the refinement of simulations
for the different normal modes.

We see convergence parameters a little different from the previous
case:
\begin{itemize}
\item only 25 cell diameters per wavelength are needed, to get an
error of around 1\%, and
\item the convergence rate is a little lower: 1.65.
\end{itemize}

Again, the gray thick line correspond to N to the power minus 1.65.}
\end{frame}

\section{Examples}
\label{sec:org05e1136}

\begin{frame}[label={sec:orgd2200dc}]{Examples: typical acoustics systems}
\begin{columns}
\begin{column}{0.50\columnwidth}
\raisebox{0.3in}{
\parbox{0.8in}{Diffraction from a \\ corner}
}
\includegraphics[width=1.8in]{images/difraction-corner-1.pdf}

\vspace{20pt}

\raisebox{0.5in}{
\parbox{1.5in}{Normal modes in a rectangular room}
}
\includegraphics[width=1.1in]{images/normal-modes-rectangular-room-analytic-comparison-1.pdf}
\end{column}

\begin{column}{0.50\columnwidth}
\raisebox{0.3in}{
\parbox{0.8in}{Pulsating cylinder}
}
\includegraphics[width=1.8in]{images/pulsating-cylinder-cartesian-1.pdf}

\vspace{20pt}

\raisebox{0.5in}{
\parbox{0.8in}{Pulsating sphere (3D)}
}
\includegraphics[height=1.2in]{images/pulsating-sphere-gmsh-1.pdf}
\end{column}
\end{columns}

\note{\label{orgb8a5da4}
\vspace{-10pt}

Having an idea of the magnitude of the error depending on the
mesh refinement. We apply the code to other acoustic systems.

There are currently 25 examples in the repository, some of which
are very similar to each other, but have a little change to make
emphasis on the impact of some detail.

Here we see:
\begin{itemize}
\item A Gaussian beam coming from the left, parallel to a surface,
encountering a corner, and diffracting.
\item Below, normal modes in a rectangular enclosure.
\item On the top right, a circle with a vibrating boundary condition,
in the center of a plane, and anechoic layers over the rest of
the walls. This system correspond to a pulsating cylinder.
\item And last, a pulsating sphere, very similar to the previous case,
but on full 3D.
\end{itemize}}
\end{frame}

\begin{frame}[label={sec:orgea53c98}]{Helmholtz resonator}
\begin{columns}
\begin{column}{0.20\columnwidth}
The bottom wall of the cavity is vibrating, with constant
amplitude.
\end{column}

\begin{column}{0.80\columnwidth}
\vspace{-20pt}

\begin{center}
\includegraphics[width=\textwidth]{images/helmholtz-resonator-vibrating-wall-1.png}
\end{center}
\end{column}
\end{columns}

\note{\label{org0d21f78}
Another example of a typical acoustics system is the Helmholtz
resonator.

In this case, there is a vibrating wall at the bottom of the
enclosure.

We see the expected increase for amplitude at the resonance
frequency.

We see, also, the radiation has much lower pressure amplitude than the
one in the enclosure.

Finally, we can see the velocity, which is related to the gradient
of the pressure, is much higher at the neck. And, there are small
regions (here, and here) corresponding to the end corrections for
the neck length.}
\end{frame}

\begin{frame}[label={sec:org30cea1e}]{Oblique incidence, reflection and refraction}
\begin{columns}
\begin{column}{0.20\columnwidth}
Two media having different speed of sound.

\vspace{10pt}

Ortega-Aguilar, Alejandro, et al. Proceedings of Meetings on
Acoustics 179ASA, 2020.
\end{column}

\begin{column}{0.80\columnwidth}
\vspace{-20pt}

\begin{center}
\includegraphics[width=\textwidth]{images/oblique-incidence-1.png}
\end{center}
\end{column}
\end{columns}

\note{\label{org9fb1273}
In this example we have a Gaussian beam. We are interested in
preventing eventual reflections in the left boundary, that's why
we have this special geometry.

At his point, the beam encounters a second media, with a different speed
of sound. Then, a reflection, and a refraction beam are
generated. Both finally dissipate inside anechoic layers.

This is a two-dimensional simulation. However, we are using a
similar 3D simulation to study the possibility of measuring speed
of sound from a experimental setup like this.

An important limitation of the code, for this case, is that
different media cannot have different densities. It is interesting
how density is not in the Helmholtz equation, but it is an
important aspect of the solution of this case. Then, it should be
a kind of boundary condition, which we think in this case leads to
a Discontinuous Galerkin Method (DGM), which FEniCS can handle,
and we are working on it.}
\end{frame}

\begin{frame}[label={sec:org21387fe}]{Horn (axisymmetric)}
\begin{columns}
\begin{column}{0.20\columnwidth}
\begin{itemize}
\item Resonance frequencies can be sharply recognized

\item Directional patterns depending on frequency are also observed
\end{itemize}
\end{column}

\begin{column}{0.80\columnwidth}
\vspace{-20pt}

\begin{center}
\includegraphics[width=\textwidth]{images/horn-1.png}
\end{center}
\end{column}
\end{columns}

\note{\label{org4493ae8}
This is an axisymmetric example consisting of a long duct,
excited in one end and having a horn in the other end.

In this case, we can see two important aspects, 
\begin{itemize}
\item the effective duct length, which we calculate from the resonance
frequencies, and
\item the directional radiation patterns, depending also on
frequency.
\end{itemize}

We are currently studying, the role of the horn geometry over
these two points.}
\end{frame}

\begin{frame}[label={sec:orgbc2f753}]{Leaky Wave Antenna (axisymmetric)}
\begin{columns}
\begin{column}{0.20\columnwidth}
O Bustamante, et al., 179th Meeting of the Acoustical Society of
America, 2020.

\vspace{10pt}

Including membranes in the simulation would be good, but it is
not yet ready (DGM).
\end{column}

\begin{column}{0.80\columnwidth}
\vspace{-20pt}

\begin{center}
\includegraphics[width=\textwidth]{images/acoustic-leaky-wave-antenna-1.png}
\end{center}
\end{column}
\end{columns}

\note{\label{orgb183100}
Finally, this system is a cylindrical duct with slits, in such a
manner that the interference of the radiation corresponding to
individual slits creates a total directional radiation.

This a Masters dissertation completed this year, presented in this
meeting last year, as well. We are currently working on optimizing
the geometry.

This is another illustration on how the implementation of a
Discontinuous Galerkin Method would benefit this code. In this
case it would allow jumps in the numeric solution, which can model
the presence of membranes inside the duct, as the ones used in
experimental devices.}
\end{frame}

\section{Conclusions}
\label{sec:org6cc0ac1}

\begin{frame}[label={sec:orge60cfa9}]{Conclusions}
\begin{itemize}
\item The implemented weak form contemplates a variety of useful
situations.
\begin{itemize}
\item Propagation, diffraction, absorption, resonance, reflection,
etc.
\end{itemize}

\item Four convergence tests were performed.
\begin{itemize}
\item The convergence rate found is between 1.65 an 2 (second order).
\item Validated (cartesian, and axisymmetric).
\end{itemize}

\item When simulating for a real application. How many cell diameters
are needed per wavelegth to get an error around 1\%?
\begin{itemize}
\item It depends on the specifics of the system, from 25 to 80.
\end{itemize}

\item Code
\begin{itemize}
\item Code is available for: solver, examples and convergence tests.
\item Many features come from the FEniCS and Paraview base code.
\item Discussion is open in the issues section of the repository.
\end{itemize}
\end{itemize}

\note{\label{orgaa5181e}
\vspace{-15pt}
Finally, here are some conclusion remarks.
\vspace{-15pt}
\begin{itemize}
\item (first) The implemented weak form contemplates a variety of useful
situations.
\begin{itemize}
\item Propagation, diffraction, absorption, resonance, reflection,
and some other.
\end{itemize}

\item (second) Four convergence tests were performed.
\begin{itemize}
\item The convergence rate found is between 1.65 an 2, which is close
to results that could be found in a second order method.
\item We consider this code to be validated, for the cartesian, and
axisymmetric cases.
\end{itemize}

\item (third) When simulating for a real application. How many cell diameters
are needed per wavelegth to get an error around 1\%?
\begin{itemize}
\item It depends on the specifics of the system, from 25 to 80. We
know this can be improved, and we are working on it.
\end{itemize}

\item (fourth) 
\begin{itemize}
\item The code is available for: solver, examples and convergence tests.
\item Many features come from the FEniCS and Paraview base code. The
engine of the numeric method is from FEniCS, and the
visualization is from paraview.
\item Finally, we want to say that the discussion, over any aspect of
this code, is open in the "issues" section of the repository.
\end{itemize}
\end{itemize}}
\end{frame}

\appendix
\begin{frame}[label={sec:orge6f9e95}]{\label{orgeedf0d4}}
\vspace{-35pt}

\begin{columns}
\begin{column}{1\columnwidth}
\begin{block}{\Huge{Thanks!}}
\vspace{15pt}

\parbox{\textwidth}{. \hspace{40pt} \Large{HEF-Acoustics} \\ . \hspace{40pt} \url{https://gitlab.com/cfd-pizca/hef-acoustics}}

\vspace{15pt}

\begin{center}
\includegraphics[width=1in]{images/unam.png}
\hspace{20pt}
\includegraphics[width=0.8in]{images/icat.png}
\hspace{20pt}
\includegraphics[width=2in]{images/asa.png}
\end{center}

\vspace{4pt}

\footnotesize{181st Meeting of the Acoustical Society of America, Seattle, Washington, November 30, 2021.}
\end{block}
\end{column}
\end{columns}

\note{\label{org51e0511}
Thank you very much.}
\end{frame}
\end{document}
