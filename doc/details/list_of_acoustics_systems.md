# Examples for acoustics systems

## Propagation

### Plane wave

- [plane-wave](demo/plane-wave):\
  <img src="demo/plane-wave/results/screenshot_1.png" height="120">

### Two plane waves at different angles

- [two-plane-waves](demo/two-plane-waves):\
  <img src="demo/two-plane-waves/results/screenshot_1.png" height="120">
  <img src="demo/two-plane-waves/results/screenshot_2.png" height="120">
  <img src="demo/two-plane-waves/results/screenshot_3.png" height="120">
  <img src="demo/two-plane-waves/results/screenshot_4.png" height="120">
  <img src="demo/two-plane-waves/results/screenshot_5.png" height="120">

### Propagating modes in a rectangular duct

- [modes-rectangular-duct](demo/modes-rectangular-duct):\
  <img src="demo/modes-rectangular-duct/results/screenshot_1.png" height="120">
  <img src="demo/modes-rectangular-duct/results/screenshot_4.png" height="120">
  <img src="demo/modes-rectangular-duct/results/screenshot_3.png" height="120">
  <img src="demo/modes-rectangular-duct/results/screenshot_5.png" height="120">

### Plane waves with velocity field 

- [velocity_check_2D](demo/velocity_check_2D):\
  <img src="demo/velocity_check_2D/results/screenshot_2.png" height="120">
  <img src="demo/velocity_check_2D/results/screenshot_3.png" height="120">
  <img src="demo/velocity_check_2D/results/screenshot_4.png" height="120">

## Diffraction

### Diffraction from a rounded corner

- [diffraction-rounded-corner](demo/diffraction-rounded-corner):\
  <img src="demo/diffraction-rounded-corner/results/screenshot_1.png" height="120">
  <img src="demo/diffraction-rounded-corner/results/screenshot_4.png" height="120">
  <img src="demo/diffraction-rounded-corner/results/screenshot_5.png" height="120">

## Radiation

### Point source

- [point-source-2D](demo/point-source-2D):\
  <img src="demo/point-source-2D/results/screenshot_1.png" height="120">
- [point-source-3D](demo/point-source-3D):\
  <img src="demo/point-source-3D/results/screenshot_1.png" height="120">

### Dipole

- [dipole](demo/dipole):\
  <img src="demo/dipole/results/screenshot_1.png" height="120">
  <img src="demo/dipole/results/screenshot_4.png" height="120">
  <img src="demo/dipole/results/screenshot_5.png" height="120">

### Pulsating cylinder

- [pulsating-cylinder-cartesian](demo/pulsating-cylinder-cartesian):\
  <img src="demo/pulsating-cylinder-cartesian/results/screenshot_1.png" height="120">

### Pulsating sphere

- [pulsating-sphere-gmsh](demo/pulsating-sphere-gmsh):\
  <img src="demo/pulsating-sphere-gmsh/results/screenshot_1.png" height="120">

### Horn

- [horn](demo/horn) (Angle dependency):\
  <img src="demo/horn/results/screenshot_4.png" height="120">
  <img src="demo/horn/results/screenshot_6a.png" height="120">
  <img src="demo/horn/results/screenshot_6b.png" height="120">

### Leaky Wave Antenna

- [acoustic-leaky-wave-antenna](demo/acoustic-leaky-wave-antenna) (Angle dependency):\
  <img src="demo/acoustic-leaky-wave-antenna/results/screenshot_7.png" height="120">
  <img src="demo/acoustic-leaky-wave-antenna/results/screenshot_1.png" height="120">
  <img src="demo/acoustic-leaky-wave-antenna/results/screenshot_6.png" height="120">
  <img src="demo/acoustic-leaky-wave-antenna/results/screenshot_8.png" height="120">

## Absorption

### Absorption depending on frequency for a rectangular 2D box

- [absorption-depending-on-frequency](demo/absorption-depending-on-frequency):\
  <img src="demo/absorption-depending-on-frequency/results/screenshot_1.png" height="120">
  <img src="demo/absorption-depending-on-frequency/results/screenshot_3.png" height="120">
  <img src="demo/absorption-depending-on-frequency/results/screenshot_5.png" height="120">

### Acoustic field in 3D room with absorbing patch

- [shoebox-3D-absorbing-patch-simple](demo/shoebox-3D-absorbing-patch-simple):\
  <img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_1.png" height="120">
  <img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_2.png" height="120">

### Comparison of an absorbing layer with a boundary condition for the admittance

- [absorption-wall-and-layer](demo/absorption-wall-and-layer):\
  <img src="demo/absorption-wall-and-layer/results/screenshot_1.png" height="120">
  <img src="demo/absorption-wall-and-layer/results/screenshot_2.png" height="120">
  <img src="demo/absorption-wall-and-layer/results/screenshot_4.png" height="120">
  <img src="demo/absorption-wall-and-layer/results/screenshot_5.png" height="120">

### Comparison of different options for an absorbing layer

- [absorption-layer-comparison](demo/absorption-layer-comparison):\
  <img src="demo/absorption-layer-comparison/results/screenshot_1.png" height="120">
  <img src="demo/absorption-layer-comparison/results/screenshot_2.png" height="120">
  <img src="demo/absorption-layer-comparison/results/screenshot_3.png" height="120">
  <img src="demo/absorption-layer-comparison/results/screenshot_4.png" height="120">

## Resonance

### Normal modes in 2D room

- [normal-modes-rectangular-room-analytic-comparison](demo/normal-modes-rectangular-room-analytic-comparison):\
  <img src="demo/normal-modes-rectangular-room-analytic-comparison/results/screenshot_3.png" height="120">

### Normal modes in axisymmetric domain

- [normal-modes-axisymmetric-simple-analytic-comparison](demo/normal-modes-axisymmetric-simple-analytic-comparison):\
  <img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_4.png" height="120">
  <img src="demo/normal-modes-axisymmetric-simple-analytic-comparison/results/screenshot_1b.png" height="120">

### Normal modes in irregular geometry

- [boundary-condition-for-velocity](demo/boundary-condition-for-velocity):\
  <img src="demo/boundary-condition-for-velocity/results/screenshot_1.png" height="120">
- [transmission-hole-two-rooms](demo/transmission-hole-two-rooms):\
  <img src="demo/transmission-hole-two-rooms/results/screenshot_2a.png" height="120">
  <img src="demo/transmission-hole-two-rooms/results/screenshot_2b.png" height="120">

### Helmholtz resonator

- [helmholtz-resonator-axisymmetric](demo/helmholtz-resonator-axisymmetric):\
  <img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_6.png" height="120">
  <img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_1.png" height="120">
  <img src="demo/helmholtz-resonator-axisymmetric/results/screenshot_2.png" height="120">

### Normal modes in a duct

- [horn](demo/horn):\
  <img src="demo/horn/results/screenshot_4.png" height="120">
  <img src="demo/horn/results/screenshot_5a.png" height="120">
  <img src="demo/horn/results/screenshot_5b.png" height="120">

### Acoustic field in 3D room with absorbing patch

- [shoebox-3D-absorbing-patch-simple](demo/shoebox-3D-absorbing-patch-simple):\
  <img src="demo/shoebox-3D-absorbing-patch-simple/results/screenshot_3.png" height="120">

## Reflection and refraction

### Oblique incidence

- [oblique-incidence](demo/oblique-incidence):\
  <img src="demo/oblique-incidence/results/screenshot_1.png" height="120">
  <img src="demo/oblique-incidence/results/screenshot_5.png" height="120">

## Transmission

### A wall with an irregular hole

- [transmission-hole-two-rooms](demo/transmission-hole-two-rooms):\
  <img src="demo/transmission-hole-two-rooms/results/screenshot_2a.png" height="120">
  <img src="demo/transmission-hole-two-rooms/results/screenshot_2b.png" height="120">
