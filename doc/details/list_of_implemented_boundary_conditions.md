# Types of boundary conditions

- Dirichlet
  - point
    - issue #12
    - [normal-modes-rectangular-room](demo/normal-modes-rectangular-room)
    - [normal-modes-rectangular-room-analytic-comparison](demo/normal-modes-rectangular-room-analytic-comparison)
    - [normal-modes-axisymmetric-simple](demo/normal-modes-axisymmetric-simple)
    - [normal-modes-axisymmetric-simple-analytic-comparison](demo/normal-modes-axisymmetric-simple-analytic-comparison)
    - [shoebox-3D-absorbing-patch-simple](demo/shoebox-3D-absorbing-patch-simple)
    - [point-source-2D](demo/point-source-2D)
    - [point-source-3D](demo/point-source-3D)
    - [helmholtz-resonator-point-source](demo/helmholtz-resonator-point-source)
  - wall
    - [define-regions-simple](demo/define-regions-simple)
    - [define-regions-set_subdomain](demo/define-regions-set_subdomain)
    - [define-regions-AutoSubDomain](demo/define-regions-AutoSubDomain)
    - [define-regions-SubDomain](demo/define-regions-SubDomain)
    - [absorption-depending-on-frequency](demo/absorption-depending-on-frequency)
    - [dipole](demo/dipole)
    - [absorption-wall-and-layer](demo/absorption-wall-and-layer)
    - [absorption-layer-comparison](demo/absorption-layer-comparison)
    - [oblique-incidence](demo/oblique-incidence) (non constant)
    - [plane-wave](demo/plane-wave) (non constant)
    - [diffraction-corner](demo/diffraction-corner) (non constant)
    - [diffraction-rounded-corner](demo/diffraction-rounded-corner) (non constant)
    - [helmholtz-resonator-external-excitation](demo/helmholtz-resonator-external-excitation) (non constant)
    - [modes-rectangular-duct](demo/modes-rectangular-duct) (non constant)
    - [velocity_check_2D](demo/velocity_check_2D)
- Neumann
  - [boundary-condition-for-velocity](demo/boundary-condition-for-velocity)
  - [pulsating-cylinder-cartesian](demo/pulsating-cylinder-cartesian)
  - [pulsating-sphere-simple](demo/pulsating-sphere-simple)
  - [pulsating-sphere-single-octant](demo/pulsating-sphere-single-octant)
  - [pulsating-sphere-gmsh](demo/pulsating-sphere-gmsh)
  - [helmholtz-resonator-vibrating-wall](demo/helmholtz-resonator-vibrating-wall)
  - [helmholtz-resonator-axisymmetric](demo/helmholtz-resonator-axisymmetric)
  - [horn](demo/horn)
  - [transmission-hole-two-rooms](demo/transmission-hole-two-rooms)
- Robin
  - issue #7
  - [shoebox-3D-absorbing-patch-simple](demo/shoebox-3D-absorbing-patch-simple)
  - [absorption-wall-and-layer](demo/absorption-wall-and-layer)
  - [velocity_check_2D](demo/velocity_check_2D)
- Absorbing region to get an anechoic wall
  - [define-regions-simple](demo/define-regions-simple)
  - [define-regions-set_subdomain](demo/define-regions-set_subdomain)
  - [define-regions-SubDomain](demo/define-regions-SubDomain)
  - [define-regions-AutoSubDomain](demo/define-regions-AutoSubDomain)
  - [pulsating-cylinder-cartesian](demo/pulsating-cylinder-cartesian)
  - [pulsating-sphere-simple](demo/pulsating-sphere-simple)
  - [pulsating-sphere-single-octant](demo/pulsating-sphere-single-octant)
  - [pulsating-sphere-gmsh](demo/pulsating-sphere-gmsh)
  - [point-source-2D](demo/point-source-2D)
  - [point-source-3D](demo/point-source-3D)
  - [oblique-incidence](demo/oblique-incidence) (circle)
  - [diffraction-corner](demo/diffraction-corner)
  - [diffraction-rounded-corner](demo/diffraction-rounded-corner)
  - [helmholtz-resonator-external-excitation](demo/helmholtz-resonator-external-excitation)
  - [helmholtz-resonator-point-source](demo/helmholtz-resonator-point-source)
  - [helmholtz-resonator-vibrating-wall](demo/helmholtz-resonator-vibrating-wall)
  - [helmholtz-resonator-axisymmetric](demo/helmholtz-resonator-axisymmetric)
  - [horn](demo/horn)
  - [modes-rectangular-duct](demo/modes-rectangular-duct)
  - [absorption-wall-and-layer](demo/absorption-wall-and-layer)
  - [absorption-layer-comparison](demo/absorption-layer-comparison)
  - [dipole](demo/dipole)
- Jump
  - issue #19
