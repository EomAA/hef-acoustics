Docker
========

First, install [Docker
CE](https://www.docker.com/products/docker-desktop) for your platform
(Windows, Mac or Linux).

Then you can use directly a `docker run` command like the one
below. **WARNING: We strongly advise against sharing your entire home
directory into a container**. Instead, make a logical folder for each
project, and share only this folder. For example:

```bash
mkdir "${HOME}/my-project"

cd "${HOME}/my-project"

docker run -ti -p 127.0.0.1:8000:8000 \
-v $(pwd):/home/fenics/shared \
-w /home/fenics/shared \
registry.gitlab.com/cfd-pizca/hef-acoustics/fenics
```

Additional images
------------------------

Please note `HEF-Acoustics` is looking forward to work with some
popular variants of FEniCS:
- dolfin_adjoint
- dolfinx (AKA FEniCSx)
- firedrake

The choice for a variant is controlled with the variable `hef.engine`,
whose default value is `fenics`.

In addition, to use a specific variant, you need an `HEF-Acoustics`
image based on the image of this specific variant. Currently, the only
variant (other than `fenics`) implemented for `HEF-Acoustics` is
`dolfin_adjoin`. Then, you can also use the code in the present
repository code with the command

```bash
fenicsproject run registry.gitlab.com/cfd-pizca/hef-acoustics/dolfin_adjoint
```

Or, the corresponding `docker run` command, pointing to the image
`registry.gitlab.com/cfd-pizca/hef-acoustics/dolfin_adjoint`. Please,
see the **WARNING** message above.

Later, in the case a new version is available, you can update the
corresponding Docker image using a command like

```bash
docker pull registry.gitlab.com/cfd-pizca/hef-acoustics/dolfin_adjoint:latest
```

Non Docker based systems
================================

The recomended way to install in these cases is simply to use `git
clone`, and the environment variable `PYHTONPATH` to let python know
where the `hef` module is.

For example, in Linux or Mac, you could use
```bash
git clone https://gitlab.com/cfd-pizca/hef-acoustics.git
cd hef-acoustics
export PYTHONPATH="$PYTHONPATH:$PWD"
cd demo/plane-wave
python3 demo.py
```

TODO: Complete this description for Windows.

Recommended installation to modify the `hef` module
=====================================================================

If you want to modify the implementation of the weak form, or the
shape functions used, you could modify the code in the `hef`
module. However, if this code correspond to the Docker container, and
this container is lost, you will loose your changes.

A simple solution is to copy the whole `hef-acoustics` directory to the `shared` directory, and modify the code at this new location
```bash
cd $HOME
cp -a -n hef-acoustics shared
```

Please see that the `-n` flag for the `cp` command is there to prevent
overwriting your existing files.

Please see that the `PYTHONPATH` variable in the `HEF-Acoustics`
Docker images (for versions 0.2.0 and later) is set to prefer a `hef`
module in the `shared` directory. Then, you shouldn't need to worry
about this.

In this scenario, to obtain updates of the code, you could use `git
pull` from your `shared/hef-acoustics` directory. However, you could
need to do some merge manually.


